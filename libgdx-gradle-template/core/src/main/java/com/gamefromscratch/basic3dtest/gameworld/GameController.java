package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

/**
 * Created by maxim.kostyuchenko on 16.03.2017.
 */
public class GameController implements InputProcessor{
    private GameWorld world;
    private PerspectiveCamera cam;
    private int selected = -1, selecting = -1;

    public GameController(GameWorld world){
        this.world = world;

    }

    public void update(float delta){
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        //https://xoppa.github.io/blog/interacting-with-3d-objects/
        selecting = getObject(screenX, screenY);
        return selecting >= 0;
    }

    public int getObject (int screenX, int screenY) {
        Ray ray = cam.getPickRay(screenX, screenY);
        int result = -1;
        float distance = -1;
        for (int i = 0; i < world.getGameObjects().size(); ++i) {
            final float dist2 = world.getGameObjects().get(i).intersects(ray);
            if (dist2 >= 0f && (distance < 0f || dist2 <= distance)) {
                result = i;
                distance = dist2;
            }
        }
        return result;
    }

    public void setSelected (int value) {
        if (selected == value) return;
        if (selected >= 0) {
            Material mat = world.getGameObjects().get(selected).getGameObjectModelInstance().materials.get(0);
            mat.clear();
            mat.set(world.getOriginalMaterial());
            world.getGameObjects().get(selected).setDrawBoundingBox(false);
        }
        selected = value;
        if (selected >= 0) {
            Material mat = world.getGameObjects().get(selected).getGameObjectModelInstance().materials.get(0);
            world.getOriginalMaterial().clear();
            world.getOriginalMaterial().set(mat);
            mat.clear();
            mat.set(world.getSelectionMaterial());
            world.getGameObjects().get(selected).setDrawBoundingBox(true);
        }
    }

    public void moveTo(Vector3 endPosition){
        if(selected >=0){
            world.getGameObjects().get(selected).moveTo(endPosition);
        }
    }

    public Vector3 intersectWithAxis(Plane plane, int screenX, int screenY){
        Vector3 intersectionPoint = new Vector3();
        Ray ray = cam.getPickRay(screenX, screenY);

        Intersector.intersectLinePlane(cam.position.x,cam.position.y,cam.position.z,ray.origin.x,ray.origin.y,ray.origin.z,plane,intersectionPoint);

        return intersectionPoint;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Gdx.app.log("GameController", "TouchUp: ["+screenX+","+screenY+"]");
        if (selecting >= 0) {
            if (selecting == getObject(screenX, screenY))
                setSelected(selecting);
            selecting = -1;
            return true;
        } else {
            if(button == Input.Buttons.RIGHT){
                moveTo(intersectWithAxis(new Plane(new Vector3(0f,0f,1f),new Vector3(1f,1f,0f)),screenX,screenY));
            }
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        Gdx.app.log("GameController", "ammount of scroll is "+amount);
        return false;
    }

    public void setCamera(PerspectiveCamera camera) {
        this.cam = camera;
    }
}

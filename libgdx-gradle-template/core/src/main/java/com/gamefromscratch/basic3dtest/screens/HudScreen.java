package com.gamefromscratch.basic3dtest.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gamefromscratch.basic3dtest.SimpleGame;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maxim.kostyuchenko on 23.03.2017.
 */
public class HudScreen implements Screen,InputProcessor {
    SimpleGame game;
    Camera cam;
    SpriteBatch spriteBatch;
    public Map<String, Texture> textures;
    private float angle = 15f;
    private TextureRegion[] 	regions = new TextureRegion[4];

    private BitmapFont font;


    public HudScreen(SimpleGame game){
        this.game = game;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Gdx.app.log("HudScreen","Touch on:"+screenX+","+screenY);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {
        this.cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.input.setInputProcessor(this);
        spriteBatch = new SpriteBatch();
        textures = new HashMap<>();
        fillTextures();
        fillFonts();
    }

    public void fillFonts(){
        font = new BitmapFont();
        font.setColor(Color.BLUE);
    }

    public void fillTextures(){
        textures.put("background", new Texture(Gdx.files.internal("assets/background.jpg")));
        textures.put("gear_ico", new Texture(Gdx.files.internal("assets/gear_ico.png")));
        textures.put("green_button_map",new Texture(Gdx.files.internal("assets/green_button_map.png")));

        regions[0] = new TextureRegion(textures.get("green_button_map"), 0, 0, 186, 52);
        regions[1] = new TextureRegion(textures.get("green_button_map"), 0, 52, 186, 52);
        regions[2] = new TextureRegion(textures.get("green_button_map"), 0, 104, 186, 52);
    }

    public void drawMenu(float delta){
        angle+=delta*100;
        /**
         * spriteBatch.draw(textures.get("gear_ico") - What Render,
         *                  64 - Where on X,
         *                  128 - Where on Y,
         *                  128 - Center of rotation, around them on Local X,
         *                  128 - Center of rotation, around them on Local Y,
         *                  256 - width, max LocalX,
         *                  256 - height, max LocalY,
         *                  0.5f - scale rate by X,
         *                  0.5f - scale rate by Y,
         *                  angle - rotation angle,
         *                  0, 0, 512, 512, false, false);
         */
        spriteBatch.draw(textures.get("gear_ico"), 16, 256, 128, 128, 256, 256, 0.5f, 0.5f, angle, 0, 0, 512, 512, false, false);
        spriteBatch.draw(regions[2],256,32);

    }

    public void drawCoords(){
        font.draw(spriteBatch,"("+Gdx.input.getX()+","+Gdx.input.getY()+")",Gdx.input.getX()+3,Gdx.graphics.getHeight()-Gdx.input.getY()-3);
    }

    @Override
    public void render(float delta) {
        setCamera(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
        spriteBatch.setProjectionMatrix(this.cam.combined);
        Gdx.gl.glClearColor(1,1,1, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin();
        drawMenu(delta);
        drawCoords();
        spriteBatch.end();
    }

    public void setCamera(float x, float y){
        this.cam.position.set(x, y,0);
        this.cam.update();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);
        spriteBatch.dispose();
        font.dispose();
        textures.clear();
    }
}

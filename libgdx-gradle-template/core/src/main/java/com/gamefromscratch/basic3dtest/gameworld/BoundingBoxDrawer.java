package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder;
import com.badlogic.gdx.math.collision.BoundingBox;

/**
 * Created by maxim.kostyuchenko on 23.03.2017.
 */
public class BoundingBoxDrawer {
    private ModelInstance mi;
    private ModelBuilder mb;
    private Model model;
    public BoundingBoxDrawer(BoundingBox bb){
        drawBoundingBox(bb);
    }

    public BoundingBoxDrawer(){}

    public void drawBoundingBox(BoundingBox bb){
        if(bb!=null){
            mb = new ModelBuilder();

            mb.begin();
            mb.node().id = "box";
            //Primitive type define how to draw object - carcass,filled,vertex
            BoxShapeBuilder.build(mb.part("box", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.RED)))
                    ,bb);

            model = mb.end();
            mi = new ModelInstance(model,"box");
        }
    }

    public ModelInstance getDrawedBoundingBox(BoundingBox bb){
        drawBoundingBox(bb);
        return mi;
    }

    public ModelInstance getDrawedBoundingBox(){ return mi;}


}

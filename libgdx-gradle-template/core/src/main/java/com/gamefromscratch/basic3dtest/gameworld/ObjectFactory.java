package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by maxim.kostyuchenko on 20.03.2017.
 */
public class ObjectFactory {
    private static ModelBuilder modelBuilder = new ModelBuilder();


    public static GameObject createObject(String name) {
        GameObject shape = null;

        if ("BOX".equals(name)) {

        }
        if ("GREEN_BOX".equals(name)) {
            shape = createGreenBox();
        }

        return shape;
    }


    private static GameObject createBox(Material material, Vector3 startPosition) {
        Model box = modelBuilder.createBox(5f, 5f, 5f,
                material,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);

        return new GameObject(box, startPosition);
    }

    private static GameObject createGreenBox() {
        return createBox(new Material(ColorAttribute.createDiffuse(Color.GREEN)), new Vector3(0f, 0f, 0f));
    }
}

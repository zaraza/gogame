package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxim.kostyuchenko on 16.03.2017.
 */
public class GameWorld {
    private Model model;
    private List<GameObject> gameObjects = new ArrayList<>();
    private List<ModelInstance> gameObjectsInstances = new ArrayList<>();
    public Model axesModel;
    public ModelInstance axesInstance;

    final float GRID_MIN = -100f;
    final float GRID_MAX = 100f;
    final float GRID_STEP = 1f;

    private Material selectionMaterial = new Material(ColorAttribute.createDiffuse(Color.FOREST));
    private Material originalMaterial = new Material();

    public GameWorld() {
        ModelBuilder modelBuilder = new ModelBuilder();
        gameObjects = new ArrayList<GameObject>();

        Model greenBox = modelBuilder.createBox(5f, 5f, 5f,
                new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);

        Model greenCapsule = modelBuilder.createCapsule(3f,9f,12,new Material(ColorAttribute.createDiffuse(Color.CORAL)),VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        Model blueSphere = modelBuilder.createSphere(4f,4f,4f,24,12,new Material(ColorAttribute.createAmbient(Color.BLUE)),VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);


        gameObjects.add(new GameObject(greenBox,new Vector3(0f,0f,15f), new BoxShape(new BoundingBox())));

        gameObjects.add(new GameObject(greenCapsule,new Vector3(15f,0f,0f),new BoxShape(new BoundingBox())));
        gameObjects.add(new GameObject(blueSphere, new Vector3(0f, 15f,0f), new BoxShape(new BoundingBox())));
        createAxes();

    }



    private void createAxes () {
        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        MeshPartBuilder builder = modelBuilder.part("grid", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        builder.setColor(Color.LIGHT_GRAY);
        for (float t = GRID_MIN; t <= GRID_MAX; t += GRID_STEP) {
            builder.line(t, GRID_MIN, 0, t, GRID_MAX, 0);
            builder.line(GRID_MIN, t, 0, GRID_MAX, t, 0);
        }
        builder = modelBuilder.part("axes", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        //X axe
        builder.setColor(Color.RED);
        builder.line(0, 0, 0, 100, 0, 0);
        //Y axe
        builder.setColor(Color.GREEN);
        builder.line(0, 0, 0, 0, 100, 0);
        //Z axe
        builder.setColor(Color.BLUE);
        builder.line(0, 0, 0, 0, 0, 100);
        axesModel = modelBuilder.end();
        axesInstance = new ModelInstance(axesModel);
    }

    public void update(float delta) {
        for(GameObject go:gameObjects){
            go.update(delta);
        }
    }

    public void dispose(){
        model.dispose();
        axesModel.dispose();
        gameObjects.clear();
        gameObjectsInstances.clear();
    }

    public Model getModel() {
        return model;
    }

    public List<ModelInstance> getGameObjectsInstances() {
        if(gameObjects.size()!=gameObjectsInstances.size()){
            for(GameObject go : gameObjects){
                gameObjectsInstances.add(go.getGameObjectModelInstance());
            }
        }

        return gameObjectsInstances;
    }

    public List<GameObject> getGameObjects(){ return this.gameObjects;}

    public Material getSelectionMaterial() {
        return selectionMaterial;
    }

    public Material getOriginalMaterial() {
        return originalMaterial;
    }
}

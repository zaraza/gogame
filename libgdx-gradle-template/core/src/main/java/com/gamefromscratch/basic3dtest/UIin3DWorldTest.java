package com.gamefromscratch.basic3dtest;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.SphereShapeBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;

/**
 * Created by maxim.kostyuchenko on 23.03.2017.
 */
public class UIin3DWorldTest implements ApplicationListener {
    //http://badlogicgames.com/forum/viewtopic.php?f=11&t=4748

    protected PerspectiveCamera cam;
    protected CameraInputController camController;
    protected ModelBatch modelBatch;
    protected Array<ModelInstance> instances;
    protected Environment environment;

    protected Model model;

    protected HUD hud;
    public AssetManager assets;

    public static class HUD {
        public Stage stage;
        public SpriteBatch batch;
        public Skin skin;


        public HUD(){
            batch = new SpriteBatch();
            stage = new Stage();
            skin = new Skin();

            Pixmap pixmap = new Pixmap(100, 100, Pixmap.Format.RGBA8888);
            pixmap.setColor(Color.RED);
            pixmap.fill();

            skin.add("white", new Texture(pixmap));

            skin.add("default", new BitmapFont());

            TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
            textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
            textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
            textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
            textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
            textButtonStyle.font = skin.getFont("default");
            skin.add("default", textButtonStyle);




            Table table = new Table();
            table.setPosition(100,100);
            stage.addActor(table);


            final TextButton button = new TextButton("Click me!", skin);
            table.add(button);

            table.add(new Image(skin.newDrawable("white", Color.RED))).size(64);

        }

    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        modelBatch = new ModelBatch();

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(3f, 7f, 10f);
        cam.lookAt(0, 4f, 0);
        cam.far = 300f;
        cam.update();

        hud = new HUD();

        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(new InputMultiplexer(hud.stage,camController));
        instances = new Array<ModelInstance>();
        assets = new AssetManager();
        assets.getLogger().setLevel(Logger.DEBUG);
        assets.load("terrain.g3db", Model.class);
        assets.finishLoading();

        initObjects();
        //hud.stage.getViewport().setScreenPosition(200,0);



    }

    public void initObjects(){
        ModelBuilder mb = new ModelBuilder();

        mb.begin();
        mb.node().id = "sphere";
        SphereShapeBuilder.build(mb.part("sphere", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.GREEN)))
                ,3f, 3f, 3f, 10, 10);

        mb.node().id = "boxe";
        BoxShapeBuilder.build(mb.part("box", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.RED)))
                ,3f, 1f, 3f);

        model = mb.end();



        instances.add(new ModelInstance(model,"sphere","boxe"));

        model = assets.get("terrain.g3db",Model.class);
        instances.add(new ModelInstance(model,new Vector3(12f,10f,11f)));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        assets.update();
        camController.update();

        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1.f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        modelBatch.begin(cam);
        modelBatch.render(instances, environment);
        modelBatch.end();

        hud.stage.getCamera().update();
        hud.stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        hud.stage.draw();


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        modelBatch.dispose();
    }
}

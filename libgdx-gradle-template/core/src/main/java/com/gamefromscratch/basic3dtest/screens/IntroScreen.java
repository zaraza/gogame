package com.gamefromscratch.basic3dtest.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gamefromscratch.basic3dtest.SimpleGame;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maxim.kostyuchenko on 15.03.2017.
 */
public class IntroScreen implements Screen,InputProcessor {
    SimpleGame game;

    float CAMERA_WIDTH = 800F;
    float CAMERA_HEIGHT = 480F;
    public float ppuX;
    public float ppuY;
    private SpriteBatch spriteBatch;
    public Map<String, Texture> textures;
    private int width, height;
    public OrthographicCamera cam;
    private Texture backGroundTexture;
    private boolean playBtn;
    private boolean checkExperiments;


    public IntroScreen(SimpleGame game){
        this.game = game;
    }

    @Override
    public void show() {
        textures = new HashMap<String, Texture>();
        ppuX = (float)width / CAMERA_WIDTH;
        ppuY = (float)height / CAMERA_HEIGHT;
        spriteBatch = new SpriteBatch();
        this.cam = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        Gdx.input.setInputProcessor(this);
        //spriteBatch.draw(backGroundTexture,0, -32, 1024 , 512);
        fillTextures();
    }

    @Override
    public void render(float delta) {
        setCamera(CAMERA_WIDTH/2, CAMERA_HEIGHT / 2f);
        spriteBatch.setProjectionMatrix(this.cam.combined);
        Gdx.gl.glClearColor(0,0,0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        //Gdx.gl.glViewport(0, 0,1200,480);


        spriteBatch.begin();
        fillBackGround();
        drawMenu();
        spriteBatch.end();
    }

    public void setCamera(float x, float y){
        this.cam.position.set(x, y,0);
        this.cam.update();
    }

    private void fillTextures(){

        backGroundTexture = new Texture(Gdx.files.internal("assets/background.jpg"));
        /**
         * Load other textures
         */
        textures.put("play_btn", new Texture(Gdx.files.internal("assets/play_btn.png")));
        textures.put("scientist_btn", new Texture(Gdx.files.internal("assets/scientist_btn.png")));

    }

    public void setSize (int w, int h) {
        this.width = w;
        this.height = h;
        ppuX = (float)width / CAMERA_WIDTH;
        ppuY = (float)height / CAMERA_HEIGHT;
    }

    public void fillBackGround(){
        spriteBatch.draw(backGroundTexture,0, -32, 1024 , 512);

    }

    public void drawMenu(){

        spriteBatch.draw(textures.get("play_btn"),10, 5, 150 , 153);
        spriteBatch.draw(textures.get("scientist_btn"), 10, 165, 150,150);
    }

    @Override
    public void resize(int width, int height) {
        setSize(width,height);
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        System.out.println("["+screenX/ppuX+","+(height-screenY)/ppuY+"]");
        if((height-screenY)/ppuY >= 21 && (height-screenY)/ppuY <= 164 && screenX/ppuX>=8.75 && screenX/ppuX<=82.5)
        playBtn = true;

        if((height-screenY)/ppuY >= 170 && (height-screenY)/ppuY <= 310 && screenX/ppuX>=8.75 && screenX/ppuX<=82.5)
            checkExperiments = true;

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if(playBtn){
            dispose();
            game.setScreen(game.gameScreen);
        }

        if(checkExperiments){
            dispose();
            game.setScreen(game.hudScreen);
        }

        playBtn=false;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }



    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);
        try{
            spriteBatch.dispose();
            backGroundTexture.dispose();
            textures.clear();
        }
        catch(Exception e){

        }
    }
}

package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;

/**
 * Created by maxim.kostyuchenko on 20.03.2017.
 */
public class GameObject {
    private ModelInstance gameObjectModelInstance;
    private Vector3 position = new Vector3();
    private Vector3 velocity = new Vector3();
    private Vector3 acceleration = new Vector3(15f,15f,0f);
    private Vector3 endPosition = new Vector3();
    public boolean drawBoundingBox = false;
    private BoundingBoxDrawer boundingBoxDrawer;

    protected Shape shape;

    public GameObject(ModelInstance modelInstance){
        this.gameObjectModelInstance = modelInstance;
    }

    public GameObject (Model model, String rootNode, boolean mergeTransform) {
        this(model, rootNode, mergeTransform, new Vector3(0f,0f,0f));

    }

    public GameObject (Model model, String rootNode, boolean mergeTransform, Vector3 translation) {
        gameObjectModelInstance = new ModelInstance(model, rootNode, mergeTransform);
        gameObjectModelInstance.transform.setToTranslation(translation);
        gameObjectModelInstance.transform.getTranslation(position);
        gameObjectModelInstance.transform.getTranslation(endPosition);

    }

    public GameObject (Model model, Vector3 translation) {
        gameObjectModelInstance = new ModelInstance(model,translation);
        gameObjectModelInstance.transform.setToTranslation(translation);
        gameObjectModelInstance.transform.getTranslation(position);
        gameObjectModelInstance.transform.getTranslation(endPosition);
    }

    public GameObject (Model model, Vector3 translation, Shape shape) {
        gameObjectModelInstance = new ModelInstance(model,translation);
        gameObjectModelInstance.transform.setToTranslation(translation);
        gameObjectModelInstance.transform.getTranslation(position);
        gameObjectModelInstance.transform.getTranslation(endPosition);

        if(shape!=null){
            this.shape = shape;
            BoundingBox box = new BoundingBox();
            gameObjectModelInstance.calculateBoundingBox(box);
            this.shape.initBounds(box);
        }
    }

    public void update(float delta){
        if(!position.equals(endPosition)) {
            Gdx.app.log("GameObject", "delta:"+delta);
            Vector3 direction = endPosition.cpy().sub(position);
            Gdx.app.log("GameObject", "direction:"+direction);
            velocity.add(acceleration.cpy().scl(delta));
            Gdx.app.log("GameObject", "velocity:"+velocity);

            Gdx.app.log("GameObject", "position from:"+position);
            position.add(direction.cpy().scl(delta));
            Gdx.app.log("GameObject", "position to:"+position);
            gameObjectModelInstance.transform.setToTranslation(position);
        }
    }

    public void moveTo(Vector3 endPosition){
        this.endPosition = endPosition;
    }

    public  float intersects(Ray ray){
        return shape == null ? -1f : shape.intersects(gameObjectModelInstance.transform, ray);
    }

    public void setShape(Shape shape){ this.shape = shape; }

    public void setDrawBoundingBox(boolean draw){
        this.drawBoundingBox = draw;
        if(draw){
            ModelBuilder mb = new ModelBuilder();
            Model model = new Model();
            mb.begin();
            mb.node().id = "3d_frame";
            //Primitive type define how to draw object - carcass,filled,vertex
            BoxShapeBuilder.build(mb.part("box", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.GREEN)))
                    ,((BaseShape)shape).boundingBox);

            model = mb.end();

            gameObjectModelInstance.nodes.add(model.getNode("3d_frame"));
        } else {
            Node n;
            for(int i=0;i<gameObjectModelInstance.nodes.size;i++){
                n = gameObjectModelInstance.nodes.get(i);
                if(n.id.equals("3d_frame")){
                    gameObjectModelInstance.nodes.removeIndex(i);
                }
            }
        }
    }



    public ModelInstance getGameObjectModelInstance(){
        return gameObjectModelInstance;
    }

}

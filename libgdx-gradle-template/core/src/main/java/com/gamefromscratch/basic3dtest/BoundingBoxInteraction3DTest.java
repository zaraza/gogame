package com.gamefromscratch.basic3dtest;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder;
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.SphereShapeBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;

/**
 * Created by maxim.kostyuchenko on 23.03.2017.
 */
public class BoundingBoxInteraction3DTest implements ApplicationListener {


    protected PerspectiveCamera cam;
    protected CameraInputController camController;
    protected ModelBatch modelBatch;
    protected Array<ModelInstance> instances;
    protected Environment environment;

    protected Model model;

    @Override
    public void create() {
        modelBatch = new ModelBatch();

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(3f, 7f, 10f);
        cam.lookAt(0, 4f, 0);
        cam.update();

        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(camController);
        instances = new Array<ModelInstance>();
        initObjects();
    }

    public void initObjects(){
        ModelBuilder mb = new ModelBuilder();

        mb.begin();
        mb.node().id = "sphere";
        SphereShapeBuilder.build(mb.part("sphere", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.GREEN)))
                ,3f, 3f, 3f, 10, 10);

        model = mb.end();




        instances.add(new ModelInstance(model,"sphere"));
        Vector3 startPosition = new Vector3(0,0,0);
        instances.get(0).transform.setTranslation(startPosition);
        BoundingBox out = new BoundingBox();
        model.calculateBoundingBox(out);

        mb.begin();
        mb.node().id = "box";
        //Primitive type define how to draw object - carcass,filled,vertex
        BoxShapeBuilder.build(mb.part("box", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.RED)))
                ,out);

        model = mb.end();

        instances.get(0).nodes.add(model.getNode("box"));
        instances.get(0).transform.setTranslation(new Vector3(4f,3f,2f));
        //Gdx.app.log("BoundingBox3DTest", "dimension for BB:"+dim);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        camController.update();

        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1.f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        modelBatch.begin(cam);
        modelBatch.render(instances, environment);
        modelBatch.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        modelBatch.dispose();
    }


}

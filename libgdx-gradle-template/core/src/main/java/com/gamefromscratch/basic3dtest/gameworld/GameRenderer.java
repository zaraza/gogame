package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by maxim.kostyuchenko on 16.03.2017.
 */
public class GameRenderer {
    public PerspectiveCamera cam;
    public ModelBatch modelBatch;
    public Environment environment;
    public CameraInputController camController;
    private GameWorld gameWorld;

    public GameRenderer(GameWorld gameWorld){
        this.gameWorld = gameWorld;

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.9f, 0.1f, 0.4f, 2f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        modelBatch = new ModelBatch();

        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.rotate(new Vector3(10f,10f,10f),120f);
        cam.position.set(35f, 35f, 35f);
        cam.lookAt(0,0,0);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        camController = new CameraInputController(cam);
       // Cursor customCursor = Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("cursor.png")), hotspotX, hotspotY);

    }

    public void render() {
        //Gdx.app.log("GameRenderer", "render");
        camController.update();
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        modelBatch.begin(cam);
        modelBatch.render(gameWorld.getGameObjectsInstances(), environment);
        modelBatch.render(gameWorld.axesInstance,environment);
        modelBatch.end();
    }

    public void dispose(){
        modelBatch.dispose();
        gameWorld.dispose();
    }

    public PerspectiveCamera getCam() {
        return cam;
    }

    public CameraInputController getCamController() {
        return camController;
    }
}

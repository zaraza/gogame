package com.gamefromscratch.basic3dtest;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxim.kostyuchenko on 10.03.2017.
 */
public class Basic3DTest implements ApplicationListener {
    public PerspectiveCamera cam;
    public List<ModelInstance> instances;
    public ModelBatch modelBatch;
    public Environment environment;
    public CameraInputController camController;
    public Cursor customCursor;


    final float GRID_MIN = -100f;
    final float GRID_MAX = 100f;
    final float GRID_STEP = 2f;

    @Override
    public void create() {
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.9f, 0.1f, 0.4f, 2f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        instances = new ArrayList<>();
        modelBatch = new ModelBatch();

        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        initCameraLocation();
        cam.update();

        camController = new CameraInputController(cam);

        Gdx.input.setInputProcessor(camController);
        //Cursor customCursor = Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("assets/cursor.png")),40, 40);
        //Gdx.graphics.setCursor(customCursor);

        createAxes();
    }

    protected void initCameraLocation(){
        cam.position.set(10f, 10f, 10f);
        cam.rotate(new Vector3(10f,10f,10f),120f);
        cam.lookAt(0,0,0);
        cam.near = 1f;
        cam.far = 300f;
    }

    public void createBox(){
        ModelBuilder modelBuilder = new ModelBuilder();
        Model model = modelBuilder.createBox(5f, 5f, 5f,
                new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
       instances.add(new ModelInstance(model));
    }

    protected void createAxes () {
        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        MeshPartBuilder builder = modelBuilder.part("grid", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        builder.setColor(Color.LIGHT_GRAY);
        for (float t = GRID_MIN; t <= GRID_MAX; t += GRID_STEP) {
            builder.line(t, GRID_MIN, 0, t, GRID_MAX, 0);
            builder.line(GRID_MIN, t, 0, GRID_MAX, t, 0);
        }
        builder = modelBuilder.part("axes", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        //X axe
        builder.setColor(Color.RED);
        builder.line(0, 0, 0, 100, 0, 0);
        //Y axe
        builder.setColor(Color.GREEN);
        builder.line(0, 0, 0, 0, 100, 0);
        //Z axe
        builder.setColor(Color.BLUE);
        builder.line(0, 0, 0, 0, 0, 100);
        Model axesModel = modelBuilder.end();
        instances.add(new ModelInstance(axesModel));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        camController.update();
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        modelBatch.begin(cam);
        modelBatch.render(instances,environment);
        modelBatch.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
       // customCursor.dispose();
        modelBatch.dispose();
        for(ModelInstance mi:instances){
            mi.model.dispose();
        }

    }
}

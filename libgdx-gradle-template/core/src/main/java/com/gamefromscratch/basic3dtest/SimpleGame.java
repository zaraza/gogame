package com.gamefromscratch.basic3dtest;

import com.badlogic.gdx.Game;
import com.gamefromscratch.basic3dtest.screens.GameScreen;
import com.gamefromscratch.basic3dtest.screens.HudScreen;
import com.gamefromscratch.basic3dtest.screens.IntroScreen;

/**
 * Created by maxim.kostyuchenko on 15.03.2017.
 */
public class SimpleGame extends Game {

    public GameScreen gameScreen;
    public IntroScreen introScreen;
    public HudScreen hudScreen;

    @Override
    public void create() {
        // TODO Auto-generated method stub

        gameScreen = new GameScreen(this);
        introScreen = new IntroScreen(this);
        hudScreen = new HudScreen(this);
        setScreen(introScreen);
    }

    @Override
    public void resize(int width, int height) {

    }
}


package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;

/**
 * Created by maxim.kostyuchenko on 22.03.2017.
 */
public abstract class BaseShape implements Shape{

    protected final static Vector3 position = new Vector3();
    public final Vector3 center = new Vector3();
    public final Vector3 dimensions = new Vector3();
    public  float radius;

    public BoundingBox boundingBox;

    public BaseShape(BoundingBox bounds){
        initBounds(bounds);
    }

    public void initBounds(BoundingBox bounds){
        boundingBox = bounds;
        bounds.getCenter(center);
        bounds.getDimensions(dimensions);
        radius = dimensions.len() / 2f;
    }

    @Override
    public boolean isVisible(Matrix4 transform, Camera cam) {
        return false;
    }

    @Override
    public float intersects(Matrix4 transform, Ray ray) {
        transform.getTranslation(position).add(center);
        if (Intersector.intersectRayBoundsFast(ray, position, dimensions)) {
            final float len = ray.direction.dot(position.x-ray.origin.x, position.y-ray.origin.y, position.z-ray.origin.z);
            return position.dst2(ray.origin.x+ray.direction.x*len, ray.origin.y+ray.direction.y*len, ray.origin.z+ray.direction.z*len);
        }
        return -1f;
    }

    @Override
    public float intersects(BoundingBox bb) {
        return 0;
    }
}

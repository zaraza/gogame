package com.gamefromscratch.basic3dtest.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.gamefromscratch.basic3dtest.Basic3DTest;
import com.gamefromscratch.basic3dtest.SimpleGame;
import com.gamefromscratch.basic3dtest.gameworld.GameController;
import com.gamefromscratch.basic3dtest.gameworld.GameRenderer;
import com.gamefromscratch.basic3dtest.gameworld.GameWorld;

/**
 * Created by maxim.kostyuchenko on 15.03.2017.
 */
public class GameScreen implements Screen {
    SimpleGame game;
    private GameWorld gameWorld;
    private GameRenderer gameRenderer;
    private GameController controller;


    public GameScreen(SimpleGame game){
        //Gdx.app.log("GameScreen", "Attached");
        this.game = game;
        gameWorld = new GameWorld();
        gameRenderer = new GameRenderer(gameWorld);



    }

    @Override
    public void show() {
        controller = new GameController(gameWorld);
        controller.setCamera(gameRenderer.getCam());
        Gdx.input.setInputProcessor(new InputMultiplexer(controller, gameRenderer.getCamController()));
    }

    @Override
    public void render(float delta) {
       // controller.update(delta);
        gameWorld.update(delta);
        gameRenderer.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        gameRenderer.dispose();
    }
}

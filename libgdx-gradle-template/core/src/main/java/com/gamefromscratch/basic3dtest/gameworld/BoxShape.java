package com.gamefromscratch.basic3dtest.gameworld;

import com.badlogic.gdx.math.collision.BoundingBox;

/**
 * Created by maxim.kostyuchenko on 23.03.2017.
 */
public class BoxShape extends BaseShape {

    public BoxShape(BoundingBox bounds) {
        super(bounds);
    }
}

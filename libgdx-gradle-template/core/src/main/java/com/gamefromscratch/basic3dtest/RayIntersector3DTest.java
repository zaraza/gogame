package com.gamefromscratch.basic3dtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

/**
 * Created by maxim.kostyuchenko on 21.03.2017.
 */
public class RayIntersector3DTest extends Basic3DTest implements InputProcessor {


    @Override
    public void create() {
        super.create();
       //
        Gdx.input.setInputProcessor(new InputMultiplexer(this,camController));
        addLine(new Vector3(2f,2f,2f),new Vector3(2f,0f,0f));
        Gdx.app.log("RayIntersector3DTest", "Instances size: ["+instances.size()+"]");
    }

    private void addLine(Vector3 start, Vector3 end){
        ModelBuilder mb = new ModelBuilder();
        mb.begin();

        MeshPartBuilder builder = mb.part("lines", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        builder.setColor(Color.GOLD);
        builder.line(start,end);
        Model line = mb.end();
        instances.add(new ModelInstance(line));
        Gdx.app.log("RayIntersector3DTest", "Instances size: ["+instances.size()+"]");
    }

    public Vector3 intersectWithAxis(Plane plane, int screenX, int screenY){
        //helpfull https://habrahabr.ru/post/131931/
        Vector3 intersectionPoint = new Vector3();
        Gdx.app.log("RayIntersector3DTest", "IntersectionPoint: ["+intersectionPoint+"]");
        Ray ray = cam.getPickRay(screenX, screenY);

        Gdx.app.log("RayIntersector3DTest", "Ray Origin: ["+ray.origin+"]");
        Gdx.app.log("RayIntersector3DTest", "Ray Direction: ["+ray.direction+"]");

        Gdx.app.log("RayIntersector3DTest", "Ray Unproject z=0: ["+cam.unproject(new Vector3(screenX,screenY,0))+"]");
        Gdx.app.log("RayIntersector3DTest", "Ray Unproject z=1: ["+cam.unproject(new Vector3(screenX,screenY,1f))+"]");
        Gdx.app.log("RayIntersector3DTest", "Cam Look At: ["+cam.direction+"]");


        Intersector.intersectLinePlane(cam.position.x,cam.position.y,cam.position.z,ray.origin.x,ray.origin.y,ray.origin.z,plane,intersectionPoint);
        Gdx.app.log("RayIntersector3DTest", "IntersectionPoint: ["+intersectionPoint+"]");

        return intersectionPoint;
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode==Input.Keys.SPACE){
            initCameraLocation();

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) {
            Gdx.app.log("RayIntersector3DTest", "Button code: ["+button+"]");
            Vector3 start = new Vector3(cam.position.x, cam.position.y, cam.position.z);
            Vector3 end = intersectWithAxis(new Plane(new Vector3(0f, 0f, 1f), new Vector3(1f,1f,0f)), screenX, screenY);
            addLine(start, end);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}

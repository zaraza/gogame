package game.go.template;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import game.go.dto.MoveDTO;
import game.go.dto.PartyDTO;

import java.io.Serializable;

/**
 * Created by Max on 02.10.2015.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME,
        include= JsonTypeInfo.As.PROPERTY,
        property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=PartyDTO.class, name="party"),
        @JsonSubTypes.Type(value=MoveDTO.class, name="move"),
})
public interface Message extends Serializable {
    /**
     * Method for identification type of incoming Message
     * Ex: Party,Move, etc.
     * Will be invoked in Factory for create Message Wrapper.
     * @return
     */
    public String getTypeOfMessage();
}

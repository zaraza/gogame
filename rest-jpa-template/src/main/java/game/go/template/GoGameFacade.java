package game.go.template;

import java.util.UUID;

/**
 * Created by Max on 02.10.2015.
 */
public interface GoGameFacade {
    /**
     * Main endpoint for incoming message
     * Process this message
     * @param message - incoming message. Design pattern command
     * @return
     */
    public Message process(Message message);
}

package game.go.template;

import java.io.Serializable;

/**
 * Created by Max on 02.10.2015.
 */
public interface Command extends Serializable {
    public abstract void execute();
    public abstract Message getResult();
}

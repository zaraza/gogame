package game.go.domain.model;

import javax.persistence.Entity;

/**
 * Created by Max on 27.09.2015.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "MOVES")
public class Move implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "move_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "move_type_id")
    private MoveType moveType;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "move_color")
    private int color;

    @Column(name = "horizontal")
    @Max(19) @Min(1)
    private int x;

    @Column(name = "vertical")
    @Max(19) @Min(1)
    private int y;

    @Column(name = "move_number")
    private int moveNumber;

    @Column(name = "move_liberty")
    private int moveLiberty;

    @Column(name = "move_time")
    private Date moveTime;

    @ManyToOne
    @JoinColumn(name = "root_move_id")
    private Move rootMove;

    @OneToMany(mappedBy = "rootMove", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Move> childMove;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "PARTY_VERSION_MOVE_LINK",
            joinColumns = @JoinColumn(name = "MOVE_ID"),
            inverseJoinColumns = @JoinColumn(name = "PARTY_VERSION_ID",nullable = false))
    private PartyVersion parentPartyVersion;

    public Move(){
        super();
        this.moveLiberty = 4;
    }

    public Move(int x, int y, int color) {
        super();
        this.setX(x);
        this.setY(y);
        this.setColor(color);
        this.setRootMove(this);
        this.setMoveLiberty(4);
    }

    public Move(int x, int y, int color, PartyVersion pv){
        this(x,y,color);
        this.parentPartyVersion = pv;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getMoveNumber() {
        return moveNumber;
    }

    public void setMoveNumber(int moveNumber) {
        this.moveNumber = moveNumber;
    }

    public int getMoveLiberty() {
        return moveLiberty;
    }

    public void setMoveLiberty(int moveLiberty) {
        this.moveLiberty = moveLiberty;
    }

    public Date getMoveTime() {
        return moveTime;
    }

    public void setMoveTime(Date moveTime) {
        this.moveTime = moveTime;
    }

    public Move getRootMove() {
        return rootMove==null?this:rootMove;
    }

    public void setRootMove(Move rootMove) {
        this.rootMove = rootMove;
    }

    public PartyVersion getParentPartyVersion() {
        return parentPartyVersion;
    }

    public void setParentPartyVersion(PartyVersion parentPartyVersion) {
        this.parentPartyVersion = parentPartyVersion;
    }

    public List<Move> getChildMove() {
        return (childMove==null) ? childMove = new ArrayList<>() : childMove;
    }

    public void setChildMove(List<Move> childMove) {
        this.childMove = childMove;
    }

    @Transient
    public void increaseLiberty(int l){
        this.moveLiberty +=l;
    }

    @Transient
    public void decreaseLiberty(int l){
        this.moveLiberty -=l;
        if(this.getRootMove()!=this) this.getRootMove().decreaseLiberty(l);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Move))
            return false;
        if (obj == this)
            return true;

        Move rhs = (Move)obj;
        return (rhs.getX() == this.getX() && rhs.getY() == this.getY() && rhs.getColor() == this.getColor());
    }
}
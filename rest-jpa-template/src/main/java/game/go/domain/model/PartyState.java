package game.go.domain.model;

import game.go.template.Command;
import game.go.template.Message;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Max on 02.10.2015.
 */
@Entity
@Table( name = "PARTY_STATE")
public class PartyState implements Serializable{

    @EmbeddedId
    private PartyStatePk partyStateId;

    @Column(name = "party_state")
    private String state;

    public PartyStatePk getPartyStateId() {
        return partyStateId;
    }

    public void setPartyStateId(PartyStatePk partyStateId) {
        this.partyStateId = partyStateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

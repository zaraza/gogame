package game.go.domain.model;

/**
 * Created by Max on 27.09.2015.
 */
import game.go.dto.PartyDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "PARTIES")
public class Party implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "party_id")
    private long id;

    @Column(name = "party_name")
    private String partyName;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time",nullable = true)
    private Date endTime;

    @ManyToOne(targetEntity = PartyStatus.class)
    @JoinColumn(name = "party_status_id", nullable = false)
    private PartyStatus partyStatus;

    @ManyToOne(targetEntity = PartyStatus.class)
    @JoinColumn(name = "party_prev_status_id", nullable = false)
    private PartyStatus partyPrevStatus;


    @Column(name = "session_guid", nullable = false)
    private String sessionGuid;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns(
            {
                    @JoinColumn(name = "party_status_id", referencedColumnName = "party_status_id",insertable = false,updatable = false),
                    @JoinColumn(name = "party_prev_status_id",referencedColumnName = "party_prev_status_id",insertable = false,updatable = false)
            }
    )
    private PartyState partyState;

    @OneToMany(mappedBy = "party",fetch = FetchType.LAZY)
    private List<PartyVersion> partyVersions;

   public Party() {
    }

    public Party(String partyName, Date startTime, Date endTime, PartyStatus partyStatus, PartyStatus partyPrevStatus, String sessionGuid) {
        this.partyName = partyName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.partyStatus = partyStatus;
        this.partyPrevStatus = partyPrevStatus;
        this.sessionGuid = sessionGuid;
    }

    public Party(PartyDTO partyDTO){
        this(partyDTO.getPartyName(),
                partyDTO.getStartTime(),
                partyDTO.getEndTime(),
                new PartyStatus(partyDTO.getPartyStatus()),
                new PartyStatus(partyDTO.getPartyPrevStatus()),
                partyDTO.getSessionGuid());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public PartyStatus getPartyStatus() {
        return partyStatus;
    }

    public void setPartyStatus(PartyStatus partyStatus) {
        this.partyStatus = partyStatus;
    }

    public PartyStatus getPartyPrevStatus() {
        return partyPrevStatus;
    }

    public void setPartyPrevStatus(PartyStatus partyPrevStatus) {
        this.partyPrevStatus = partyPrevStatus;
    }

    public String getSessionGuid() {
        return sessionGuid;
    }

    public void setSessionGuid(String sessionGuid) {
        this.sessionGuid = sessionGuid;
    }

    public PartyState getPartyState() {
        return partyState;
    }

    public void setPartyState(PartyState partyState) {
        this.partyState = partyState;
    }

    public List<PartyVersion> getPartyVersions() {
        return partyVersions;
    }

    public void setPartyVersions(List<PartyVersion> partyVersions) {
        this.partyVersions = partyVersions;
    }
}

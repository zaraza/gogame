package game.go.domain.model;

import javax.persistence.*;

/**
 * Created by kostyuchenko on 20.10.2015.
 */
@Entity
@Table(name = "MOVE_TYPE")
public class MoveType {

    public static final long WHITE = 1L;

    public static final long BLACK = 2L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "move_type_id")
    private long moveTypeId;

    @Column(name = "move_type_name")
    private String moveTypeName;
}

package game.go.domain.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Max on 25.10.2015.
 */
@Entity
@Table(name = "PARTY_VERSION")
public class PartyVersion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "party_version_id")
    private long partyVersionId;

    @ManyToOne(targetEntity = Party.class)
    @JoinColumn(name = "party_id",nullable = false)
    private Party party;

    @Column(name = "party_version")
    private long partyVersion;

    @Column(name = "by_user_id")
    private long byUser;

    @ManyToOne(targetEntity = PartyVersion.class)
    @JoinColumn(name = "parent_party_version_id")
    private PartyVersion parentPartyVersion;

    @OneToMany(mappedBy = "parentPartyVersion")
    List<PartyVersion> childPartyVersion;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentPartyVersion",cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Move> partyVersionMove;

    public long getPartyVersionId() {
        return partyVersionId;
    }

    public void setPartyVersionId(long partyVersionId) {
        this.partyVersionId = partyVersionId;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public long getPartyVersion() {
        return partyVersion;
    }

    public void setPartyVersion(long partyVersion) {
        this.partyVersion = partyVersion;
    }

    public long getByUser() {
        return byUser;
    }

    public void setByUser(long byUser) {
        this.byUser = byUser;
    }

    public PartyVersion getParentPartyVersion() {
        return parentPartyVersion;
    }

    public void setParentPartyVersion(PartyVersion parentPartyVersion) {
        this.parentPartyVersion = parentPartyVersion;
    }

    public List<PartyVersion> getChildPartyVersion() {
        return childPartyVersion;
    }

    public void setChildPartyVersion(List<PartyVersion> childPartyVersion) {
        this.childPartyVersion = childPartyVersion;
    }

    public List<Move> getPartyVersionMove() {
        return partyVersionMove;
    }

    public void setPartyVersionMove(List<Move> partyVersionMove) {
        this.partyVersionMove = partyVersionMove;
    }
}

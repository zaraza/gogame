package game.go.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PARTY_STATUS")
public class PartyStatus implements Serializable {

    /* Block of constants */
    public static final long CREATE      = 1L;
    public static final long WAIT        = 2L;
    public static final long IN_PROGRESS = 3L;
    public static final long FROZEN      = 4L;
    public static final long CANCELED    = 5L;
    public static final long COMPLETED   = 6L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ps_id")
    private long id;

    @Column(name = "ps_name")
    private String statusName;

    public PartyStatus() {
    }

    public PartyStatus(long id){
        this.id = id;
    }

    public PartyStatus(String statusName) {
        this.statusName = statusName;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}

package game.go.domain.model;

import game.go.dto.PartyDTO;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by kostyuchenko on 22.10.2015.
 */
@Embeddable
public class PartyStatePk implements Serializable{

    /* Block of constants */
    public static final PartyStatePk CREATE_PARTY           = new PartyStatePk(2,1);
    public static final PartyStatePk CREATE_AND_CANCEL      = new PartyStatePk(5,1);
    public static final PartyStatePk WAIT_AND_CANCEL        = new PartyStatePk(5,2);
    public static final PartyStatePk WAIT_AND_PROGRESS      = new PartyStatePk(3,2);
    public static final PartyStatePk FROZE_PARTY            = new PartyStatePk(4,3);
    public static final PartyStatePk PROGRESS_AFTER_FROZE   = new PartyStatePk(3,4);
    public static final PartyStatePk COMPLETE_PARTY         = new PartyStatePk(6,3);

    @Column(name = "party_status_id")
    private long party_status_id;
    @Column(name = "party_prev_status_id")
    private long party_prev_status_id;

    public PartyStatePk(){};
    public PartyStatePk(long partyStatusId,long partyPrevStatusId){
        super();
        this.party_status_id = partyStatusId;
        this.party_prev_status_id = partyPrevStatusId;
    }

    public PartyStatePk(PartyDTO partyDTO){
        this.party_status_id = partyDTO.getPartyStatus();
        this.party_prev_status_id = partyDTO.getPartyPrevStatus();
    }

    public long getPartyStatusId() {
        return party_status_id;
    }

    public void setPartyStatusId(long partyStatusId) {
        this.party_status_id = partyStatusId;
    }

    public long getPartyPrevStatusId() {
        return party_prev_status_id;
    }

    public void setPartyPrevStatusId(long partyPrevStatusId) {
        this.party_prev_status_id = partyPrevStatusId;
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(this == o) return true;
        if(!(o instanceof PartyStatePk)) return false;
        if((this.party_status_id)==((PartyStatePk) o).getPartyStatusId() &&
                this.party_prev_status_id == ((PartyStatePk) o).getPartyPrevStatusId()){
            return true;
        }
        return false;
    }
}

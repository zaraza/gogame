package game.go.domain.states.party;

import game.go.domain.model.Party;
import game.go.dto.PartyDTO;
import game.go.template.Command;
import game.go.template.Message;

/**
 * Created by Max on 18.11.2015.
 */
public abstract class PartyState implements Command {
    protected PartyDTO partyDTO;


    public PartyState(){}

    public PartyState(PartyDTO _partyDTO){
        this.partyDTO = _partyDTO;
    }

    public abstract Message getResult();
}

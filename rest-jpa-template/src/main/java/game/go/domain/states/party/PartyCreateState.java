package game.go.domain.states.party;

import game.go.dto.PartyDTO;
import game.go.services.PartyService;
import game.go.template.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by Max on 18.11.2015.
 */
@Component
public class PartyCreateState extends PartyState {

    private PartyService partyService;

    public PartyCreateState(){}

    public PartyCreateState(PartyDTO _partyDTO, PartyService partyService){
        super(_partyDTO);
        this.partyService = partyService;
    }

    @Override
    public void execute() {
        this.partyDTO = partyService.createParty(partyDTO);
    }

    @Override
    public Message getResult() {
        return this.partyDTO;
    }
}

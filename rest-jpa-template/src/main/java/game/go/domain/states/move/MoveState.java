package game.go.domain.states.move;

import game.go.dto.MoveDTO;
import game.go.template.Message;

/**
 * Created by Max on 27.12.2015.
 */
public abstract class MoveState {
    protected MoveDTO moveDTO;


    public MoveState(){}

    public MoveState(MoveDTO moveDTO){ this.moveDTO = moveDTO;}

    public abstract Message getResult();
}

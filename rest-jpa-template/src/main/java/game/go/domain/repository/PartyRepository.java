package game.go.domain.repository;

import game.go.domain.model.Party;
import game.go.domain.model.PartyState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Max on 02.10.2015.
 */
public interface PartyRepository extends JpaRepository<Party,Long> {
    public List<Party> findByPartyStatus_IdIn(List<Long> partyStatuses);
}

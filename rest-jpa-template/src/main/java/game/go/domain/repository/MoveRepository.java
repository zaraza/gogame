package game.go.domain.repository;

import game.go.domain.model.Move;
import game.go.domain.model.PartyVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Max on 27.09.2015.
 */
@Repository
public interface MoveRepository extends JpaRepository<Move,Long> {
    @Query("SELECT m FROM Move m WHERE m.parentPartyVersion=?3 and m.isDeleted = false and ( (m.x = ?1+1 and m.y = ?2) or (m.x = ?1-1 and m.y = ?2)" +
            "or (m.x = ?1 and m.y = ?2+1) or (m.x = ?1 and m.y = ?2-1))")
    public List<Move> getNeighbors(int x, int y, PartyVersion pv);


}

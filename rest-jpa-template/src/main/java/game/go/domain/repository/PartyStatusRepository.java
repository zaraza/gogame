package game.go.domain.repository;

import game.go.domain.model.PartyStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Max on 02.10.2015.
 */
public interface PartyStatusRepository extends JpaRepository<PartyStatus,Integer> {
}

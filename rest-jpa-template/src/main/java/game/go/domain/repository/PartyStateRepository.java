package game.go.domain.repository;

import game.go.domain.model.PartyState;
import game.go.domain.model.PartyStatePk;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Max on 02.10.2015.
 */
public interface PartyStateRepository extends JpaRepository<PartyState,PartyStatePk> {
}

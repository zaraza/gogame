package game.go.services;

import game.go.domain.model.Party;
import game.go.domain.model.PartyState;
import game.go.dto.PartyDTO;

/**
 * Created by Max on 18.11.2015.
 */
public interface PartyService {


    public PartyState getPartyState(PartyDTO partyDTO);
    public Party getParty(PartyDTO partyDTO);
    public PartyDTO createParty(PartyDTO partyDTO);
}

package game.go.services;

import game.go.domain.model.Party;
import game.go.domain.model.PartyState;
import game.go.domain.model.PartyStatePk;
import game.go.domain.repository.PartyRepository;
import game.go.domain.repository.PartyStateRepository;
import game.go.dto.PartyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Max on 18.11.2015.
 */
@Service
public class PartyServiceImpl implements PartyService{

    @Autowired
    private PartyStateRepository partyStateRepository;

    @Autowired
    private PartyRepository partyRepository;

    @Override
    public PartyState getPartyState(PartyDTO partyDTO) {
        PartyStatePk partyStatePk = new PartyStatePk(partyDTO);
        partyStateRepository.findOne(partyStatePk);
        return null;
    }

    @Override
    public Party getParty(PartyDTO partyDTO) {
        return null;
    }

    @Override
    public PartyDTO createParty(PartyDTO partyDTO) {
        Party p = new Party(partyDTO);
        p = partyRepository.save(p);
        partyDTO.setId(p.getId());
        return partyDTO;
    }
}

package game.go.services;

import game.go.domain.model.PartyStatePk;
import game.go.domain.states.party.PartyCreateState;
import game.go.dto.PartyDTO;
import game.go.services.PartyService;
import game.go.template.Command;
import game.go.template.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by Max on 02.10.2015.
 */
@Service("goGameFactory")
public class GoGameFactory {

    @Autowired
    private PartyService partyService;

    public GoGameFactory(){}

    public Command getDecoratedItem(Message message) {
        if (message == null || message.getTypeOfMessage() == null) {
            return null;
        }

        if (message.getTypeOfMessage().equalsIgnoreCase("PARTY")) {

            PartyStatePk partyStatePk = new PartyStatePk((PartyDTO)message);
            if(partyStatePk.equals(PartyStatePk.CREATE_PARTY)){

                return new PartyCreateState((PartyDTO)message,partyService);
            }

            return null;
        }

        if (message.getTypeOfMessage().equalsIgnoreCase("MOVE")) {

        }

        return null;
    }
}

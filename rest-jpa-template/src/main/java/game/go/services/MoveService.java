package game.go.services;

import game.go.domain.model.Move;
import game.go.dto.MoveDTO;

import java.util.List;

/**
 * Created by Max on 02.10.2015.
 */
public interface MoveService {
    /**
     * Analyze move and execute
     * @param move
     */
    public void makeMove(MoveDTO move);

    /**
     * Analyze move and execute
     * @param move
     */
    public void makeMove(Move move);
    /**
     * Get the Neighbors for current move
     *
     * @param move
     * @return list of Nieghbors move, independed of color
     */
    public List<Move> getNeighbors(MoveDTO move);

    /**
     * Get the Neighbors for current move
     *
     * @param move
     * @return list of Nieghbors move, independed of color
     */
    public List<Move> getNeighbors(Move move);

    /**
     * Define: is one move connected to second move.
     *
     * @param one - some move
     * @param two - some move
     * @return true - connected, false - not connected
     */
    public boolean isConnected(Move one, Move two);


    /**
     * Union of two stone groups
     * Every group is flat tree.
     * In result every of two group must have one common root move.
     * @param rootTarget - root Move of first stone group
     * @param rootSource - root Move of second stone group
     */
    public void union(Move rootTarget, Move rootSource);
}

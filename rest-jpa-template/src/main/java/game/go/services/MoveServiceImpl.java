package game.go.services;

import game.go.domain.model.Move;
import game.go.domain.repository.MoveRepository;
import game.go.dto.MoveDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Max on 31.10.2015.
 */
@Service("moveService")
@Transactional
public class MoveServiceImpl implements MoveService {

    @Autowired
    private MoveRepository moveRepository;

    @Override
    public void makeMove(MoveDTO move) {

    }

    @Override
    public void makeMove(Move move) {
        List<Move> neighbors = getNeighbors(move);
        if(neighbors!=null && !neighbors.isEmpty()) {
            for(Move mn : neighbors){
                if(mn.getColor() == move.getColor()){
                    if(!move.getRootMove().equals(move)) {
                        union(move, mn);
                    } else {
                        move.setRootMove(mn.getRootMove());
                        mn.getRootMove().getChildMove().add(move);
                        mn.getRootMove().increaseLiberty(move.getMoveLiberty());
                    }
                } else {
                    if(mn.getRootMove().getMoveLiberty()==0 && !mn.getRootMove().isDeleted()){
                        mn.getRootMove().setIsDeleted(true);
                        mn.getRootMove().getChildMove().stream().forEach(m->m.setIsDeleted(true));
                        moveRepository.save(mn.getRootMove());
                    }
                }
            }
        }
        move.getParentPartyVersion().getPartyVersionMove().add(move);
        moveRepository.save(move);
    }

    @Override
    public List<Move> getNeighbors(MoveDTO move) {
        return null;
    }

    @Override
    public List<Move> getNeighbors(Move move) {
        List<Move> neighbors = moveRepository.getNeighbors(move.getX(), move.getY(), move.getParentPartyVersion());
        if(neighbors!=null && !neighbors.isEmpty()) {
            neighbors.forEach(n -> {
                if(!n.isDeleted()) {
                    n.decreaseLiberty(1);
                    //decrease self
                    move.decreaseLiberty(1);
                }
            });
        }

        return  neighbors;
    }

    @Override
    public boolean isConnected(Move one, Move two) {
        return (one.getColor() == two.getColor()) && (one.getRootMove() == two.getRootMove());
    }

    @Override
    public void union(Move target, Move source) {
        if (!target.getRootMove().equals(source.getRootMove())) {
            for (Move m : source.getRootMove().getChildMove()) {
                    m.setRootMove(target.getRootMove());
                    target.getRootMove().getChildMove().add(m);
            }
            //and source move root assign to target move root
            target.getRootMove().increaseLiberty(source.getRootMove().getMoveLiberty());
            source.getChildMove().clear();
            source.setRootMove(target.getRootMove());
            target.getRootMove().getChildMove().add(source);
            moveRepository.save(Arrays.asList(target,source));
        }
    }
}

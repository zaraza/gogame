package game.go.dto;

import game.go.template.Message;

/**
 * Created by Max on 02.10.2015.
 */
public class MoveDTO implements Message {
    private int x;
    private int y;
    private int color;
    private MoveDTO rootMove;
    private int moveLiberty;
    private int moveNumber;

    public MoveDTO(){
        super();
    }

    public MoveDTO(int x, int y, int color){
        this.x = x;
        this.y = y;
        this.color = color;
        this.moveLiberty = 4;
    }

    public MoveDTO(int x, int y){
        this.x = x;
        this.y = y;
        this.moveLiberty = 4;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public MoveDTO getRootMove() {
        if(rootMove==null) return this;
        return rootMove;
    }

    public void setRootMove(MoveDTO rootMove) {
        this.rootMove = rootMove;
    }

    public int getMoveLiberty() {
        return moveLiberty;
    }

    public void setMoveLiberty(int moveLiberty) {
        this.moveLiberty = moveLiberty;
    }

    public int getMoveNumber() {
        return moveNumber;
    }

    public void setMoveNumber(int moveNumber) {
        this.moveNumber = moveNumber;
    }

    @Override
    public String getTypeOfMessage() {
        return "MOVE";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoveDTO moveDTO = (MoveDTO) o;

        if (x != moveDTO.x) return false;
        if (y != moveDTO.y) return false;
//        if (color != moveDTO.color) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}

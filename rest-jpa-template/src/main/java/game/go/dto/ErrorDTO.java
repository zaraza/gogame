package game.go.dto;

import game.go.template.Message;

/**
 * Created by Max on 27.12.2015.
 */
public class ErrorDTO implements Message {
    private String description;
    private Message rootMessage;


    public ErrorDTO(String description, Message rootMessage){
        this.description = description;
        this.rootMessage = rootMessage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Message getRootMessage() {
        return rootMessage;
    }

    public void setRootMessage(Message rootMessage) {
        this.rootMessage = rootMessage;
    }

    @Override
    public String getTypeOfMessage() {
        return "ERROR";
    }
}

package game.go.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import game.go.template.Message;

import java.util.Date;

/**
 * Created by Max on 02.10.2015.
 */
@JsonTypeName("party")
public class PartyDTO implements Message{

    private long id;
    private String partyName;
    private Date startTime;
    private Date endTime;

    private long partyStatus;
    private long partyPrevStatus;
    private String sessionGuid;
    private String typeOfMessage;

    public PartyDTO(){}

    public PartyDTO(long id, String partyName, Date startTime, long partyStatus, long partyPrevStatus) {
        this.id = id;
        this.partyName = partyName;
        this.startTime = startTime;
        this.partyStatus = partyStatus;
        this.partyPrevStatus = partyPrevStatus;
    }

    public PartyDTO(String partyName, Date startTime, long partyStatus, long partyPrevStatus, String sessionGuid){
        this.partyName = partyName;
        this.startTime = startTime;
        this.partyStatus = partyStatus;
        this.partyPrevStatus = partyPrevStatus;
        this.sessionGuid = sessionGuid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public long getPartyStatus() {
        return partyStatus;
    }

    public void setPartyStatus(long partyStatus) {
        this.partyStatus = partyStatus;
    }

    public long getPartyPrevStatus() {
        return partyPrevStatus;
    }

    public void setPartyPrevStatus(long partyPrevStatus) {
        this.partyPrevStatus = partyPrevStatus;
    }

    public String getSessionGuid() {
        return sessionGuid;
    }

    public void setSessionGuid(String sessionGuid) {
        this.sessionGuid = sessionGuid;
    }

    @Override
    public String getTypeOfMessage() {
        return "PARTY";
    }
}

package game.go.controllers;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import game.go.Greeting;
import game.go.config.RepositoryConfiguration;
import game.go.services.GoGameFactory;
import game.go.template.Command;
import game.go.template.GoGameFacade;
import game.go.template.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class FacadeController implements GoGameFacade{

    @Autowired
    GoGameFactory goGameFactory;


    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    @ResponseBody
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @Override
    @RequestMapping(value = "/process", method = RequestMethod.POST)
    @ResponseBody
    public Message process(@RequestBody Message message) {
        Command c = goGameFactory.getDecoratedItem(message);
        c.execute();
        return c.getResult();
    }
}
package game.go.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Created by kostyuchenko on 20.10.2015.
 */
//@Configuration
//@EnableTransactionManagement
public class DataSourceConfig {

    @Value("${spring.datasource.url}")
    private String jndiName;

    //@Bean(destroyMethod = "close")
    @Bean
    public DataSource dataSource() {
        JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
        dataSource.setJndiName(jndiName);
        dataSource.setLookupOnStartup(false);
        dataSource.setProxyInterface(DataSource.class);
        try {
            dataSource.afterPropertiesSet();
        } catch (NamingException e) {
            // rethrow
            throw new RuntimeException(e);
        }
        return (DataSource) dataSource.getObject();
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }
}

package game.go.logic;

import game.go.domain.model.Move;
import game.go.dto.MoveDTO;

import java.util.List;

/**
 * Created by Max on 27.12.2015.
 */
public interface MoveAnalyzer {

//    public void union(Move target, Move source);
    public void union(MoveDTO target, MoveDTO source, List<MoveDTO> movesList);

//    public boolean isConnected(Move one, Move two);
    public boolean isConnected(MoveDTO one, MoveDTO two, List<MoveDTO> movesList);

    public List<MoveDTO> getNeighbors(MoveDTO move,List<MoveDTO> movesList);
//    public List<Move> getNeighbors(Move move);

    public void makeMove(MoveDTO move, List<MoveDTO> movesList);
//    public void makeMove(Move move);


}

package game.go.logic;

import game.go.domain.model.Move;
import game.go.dto.MoveDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 27.12.2015.
 */
public class ClientMoveAnalyzer implements MoveAnalyzer {

    public void decreaseLiberty(MoveDTO moveDTO){
        moveDTO.setMoveLiberty(moveDTO.getMoveLiberty()-1);
        if(!moveDTO.getRootMove().equals(moveDTO)){
            moveDTO.getRootMove().setMoveLiberty(moveDTO.getRootMove().getMoveLiberty()-1);
        }
    }

    public void increaseLiberty(MoveDTO moveDTO){
        moveDTO.setMoveLiberty(moveDTO.getMoveLiberty()-1);
        if(!moveDTO.getRootMove().equals(moveDTO)){
            moveDTO.getRootMove().setMoveLiberty(moveDTO.getRootMove().getMoveLiberty()-1);
        }
    }

    /**
     *
     * @param source - additional checker to cluster
     * @param target - checher from cluster
     */
    public void increaseCluster(MoveDTO source,MoveDTO target){
//        decreaseLiberty(target);
//        decreaseLiberty(source);

        if(!target.getRootMove().equals(target)){
            target.getRootMove().setMoveLiberty(target.getRootMove().getMoveLiberty()+source.getMoveLiberty());
        } else {
            target.setMoveLiberty(target.getMoveLiberty()+source.getMoveLiberty());
        }
    }

    @Override
    public void union(MoveDTO target, MoveDTO source, List<MoveDTO> movesList) {
        if (!target.equals(source)) {
            for (MoveDTO m : movesList) {
                if (m.getRootMove().equals(source) &&
                        m.getColor() == source.getColor()) {
                    m.setRootMove(target);
                }
            }
        }
    }

    @Override
    public boolean isConnected(MoveDTO one, MoveDTO two, List<MoveDTO> movesList) {
        return (one.getColor() == two.getColor()) && (one.getRootMove() == two.getRootMove());
    }

    @Override
    public List<MoveDTO> getNeighbors(MoveDTO move, List<MoveDTO> movesList) {
        ArrayList<MoveDTO> result = new ArrayList<MoveDTO>();

        if (movesList != null && movesList.size() > 0) {

            MoveDTO m1 = new MoveDTO(move.getX() - 1, move.getY());
            MoveDTO m2 = new MoveDTO(move.getX() + 1, move.getY());
            MoveDTO m3 = new MoveDTO(move.getX(), move.getY() + 1);
            MoveDTO m4 = new MoveDTO(move.getX(), move.getY() - 1);

            if (movesList.indexOf(m1) > -1) {
                m1 = movesList.get(movesList.indexOf(m1));
                result.add(m1);
            }
            if (movesList.indexOf(m2) > -1) {
                m2 = movesList.get(movesList.indexOf(m2));
                result.add(m2);
            }
            if (movesList.indexOf(m3) > -1) {
                m3 = movesList.get(movesList.indexOf(m3));
                result.add(m3);
            }
            if (movesList.indexOf(m4) > -1) {
                m4 = movesList.get(movesList.indexOf(m4));
                result.add(m4);
            }
        }
        return result;
    }

    @Override
    public void makeMove(MoveDTO move, List<MoveDTO> movesList) {
        //����� ���� �������
        List<MoveDTO> neighbors = getNeighbors(move, movesList);
        //�������� ������ ��� � ������ �����
        movesList.add(move);
        //�������������� ������� ������
        for (MoveDTO mt : neighbors) {
            //� ����� ������ ��������� �������.
            decreaseLiberty(move);
            decreaseLiberty(mt);
            //���� ����� ���������
            if (mt.getColor() == move.getColor()) {
                //���� �� �� ���� ���� ������ � ��� ������ �� �������� ������ ������ ������ � ��� ������ �� �������� ����� �������
                if (!move.getRootMove().equals(move) && (!move.getRootMove().equals(mt)&&!move.getRootMove().equals(mt.getRootMove()))) {

                    //��������� ������� ������ �������� � ��������� ������������ ��������
                    increaseCluster(move.getRootMove(),mt.getRootMove());
                    //��������� �������� ����� ��������� ������ �����
                    union(move.getRootMove(), mt.getRootMove(), movesList);
                } else if(move.getRootMove().equals(move)) {
                    //�������� ������� ��������, � �������� ��������������
                    increaseCluster(move,mt);
                    // ��������� ������ ������ ����, �� ������ ������ ������
                    move.setRootMove(mt.getRootMove());

                }

            } else {

                if (mt.getRootMove().getMoveLiberty() == 0) {
                    System.out.println("Search for deleting!");
                    List<MoveDTO> deleted = new ArrayList<MoveDTO>();
                    for (MoveDTO em : movesList) {
                        if (em.getRootMove() == mt.getRootMove()) {
                            deleted.add(em);
                        }
                    }
                    movesList.removeAll(deleted);
                }
            }

        }
    }
}


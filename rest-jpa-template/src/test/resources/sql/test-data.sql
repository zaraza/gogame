


--Independent Dictionaries --
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('CREATE');
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('WAIT');
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('IN_PROGRESS');
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('FROZEN');
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('CANCELED');
INSERT INTO GO_GAME.PARTY_STATUS(PS_NAME) VALUES('COMPLETED');


INSERT INTO GO_GAME.MOVE_TYPE(MOVE_TYPE_NAME) VALUES('WHITE');
INSERT INTO GO_GAME.MOVE_TYPE(MOVE_TYPE_NAME) VALUES('BLACK');
INSERT INTO GO_GAME.MOVE_TYPE(MOVE_TYPE_NAME) VALUES('WHITE_DELETED');
INSERT INTO GO_GAME.MOVE_TYPE(MOVE_TYPE_NAME) VALUES('BLACK_DELETED');


INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(2,1,'CREATE_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(5,1,'CANCEL_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(5,2,'CANCEL_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(3,2,'PROGRESS_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(4,3,'FROZE_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(3,4,'PROGRESS_PARTY');
INSERT INTO GO_GAME.PARTY_STATE(party_status_id,party_prev_status_id,party_state) VALUES(6,3,'COMPLETE_PARTY');

-- End of Independent Dictionaries --

INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
    VALUES ('Test  Wait  Party 01',current_timestamp,2,1,'1111-2222-1111');
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test  Wait  Party 02',current_timestamp,2,1,'1111-2222-2222');
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test  Wait  Party 03',current_timestamp,2,1,'1111-2222-3333');


INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party With Moves1',current_timestamp,4,3,'33333-111-1111');--id 4
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (4,1,0); -- id 1
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                  VALUES (         0,          7,        7,1,3,current_timestamp,null);--id 1
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (1,1);

--Simple party with one start move
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party With Moves2',current_timestamp,4,3,'33333-111-2222');--id 5
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (5,1,0); -- id 2
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (        0,         10,       11,           1,           3,current_timestamp,null);--id 2
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (2,2);

--Simple Party for check root, common liberty, and addition moves
-- simulate simple vertical string
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party With Moves for action',current_timestamp,4,3,'33333-2222-3333');--id 6L
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (6,1,0);--id 3

INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (0        ,          9,        9,           1,           3,current_timestamp,null);--id 3
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (3,3);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES(         0,          9,       10,           2,           2,current_timestamp,   3);--id 4
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (3,4);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                    VALUES (0,               9,        11,           3,           2,current_timestamp,   3);--id 5
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (3,5);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES(         0,          9,       12,           4,           3,current_timestamp,   3);--id 6
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (3,6);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES(         0,          9,       14,           5,           4,current_timestamp,   null);--id 7
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (3,7);

--Simple Party for check root, common liberty, and addition moves
-- simulate simple vertical string
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party With Moves for union',current_timestamp,4,3,'33333-2222-4444');--id 7
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (7,1,0);--id 4

INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (0        ,          9,        9,           1,           3,current_timestamp,null);--id 8
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (4,8);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (0        ,          9,        10,           2,           3,current_timestamp,8);--id 9
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (4,9);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (0        ,          9,        12,           3,           3,current_timestamp,null);--id 10
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (4,10);
INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
                   VALUES (0        ,          9,        13,           4,           3,current_timestamp,10);--id 11
INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (4,11);

--Template for Party without moves
--for making string 5-5 5-6 5-7 5-8
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party Without Moves for Stringing',current_timestamp,4,3,'33333-2222-5555');--id 8L
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (8,1,0);--id 5

--Template for Party without moves
--for making string � figure
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party Without Moves for Cross',current_timestamp,4,3,'33333-2222-6666');--id 9L
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (9,1,0);--id 5

--Template for Party without moves
--for making string � figure
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party Without Moves for count liberty',current_timestamp,4,3,'33333-2222-7777');--id 10L
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (10,1,0);--id 5

--Template for Party without moves
--for test capturing stone,
INSERT INTO GO_GAME.PARTIES(party_name,start_time,party_status_id,party_prev_status_id,session_guid)
VALUES ('Test Party Without Moves for capture stone',current_timestamp,4,3,'33333-2222-8888');--id 11L
INSERT INTO GO_GAME.PARTY_VERSION(party_id,party_version,by_user_id) VALUES (11,1,0);--id 6
/* Create figure
         1 1 1
       1 0 0 0 1
       1 0   0 1
       1 0 0 0 1
         1 1 1
 */
--
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
--                    VALUES (1        ,        11,        10,           1,           2,current_timestamp,null);--id 12
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,12);
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
--                    VALUES (1        ,        12,        10,           2,           1,current_timestamp,12);--id 13
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,13);
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
--                    VALUES (1        ,        13,        10,           3,           2,current_timestamp,12);--id 14
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,14);
--
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
--                    VALUES (1        ,        10,        11,           4,           2,current_timestamp,null);--id 15
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,15);
--
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
--                    VALUES (0        ,        11,        11,           5,           2,current_timestamp,null);--id 16
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,16);
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
-- VALUES (1        ,        12,        10,           2,           1,current_timestamp,12);--id 17
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,17);
-- INSERT INTO GO_GAME.MOVES(move_color, horizontal, vertical, move_number,move_liberty,move_time,root_move_id)
-- VALUES (1        ,        13,        10,           3,           2,current_timestamp,12);--id 18
-- INSERT INTO GO_GAME.PARTY_VERSION_MOVE_LINK(party_version_id,move_id) VALUES (6,18);

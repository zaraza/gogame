ALTER TABLE  "GO_GAME".USER_COUNTRY_LINK
ADD CONSTRAINT FK_USER
FOREIGN KEY (user_id)
REFERENCES "GO_GAME".USER(user_id);
ALTER TABLE  "GO_GAME".USER_COUNTRY_LINK
ADD CONSTRAINT FK_COUNTRY
FOREIGN KEY (country_id)
REFERENCES "GO_GAME".COUNTRY(country_id);



ALTER TABLE "GO_GAME".MOVES
ADD CONSTRAINT FK_MOVE_TYPE
FOREIGN KEY (move_type_id)
REFERENCES "GO_GAME".MOVE_TYPE(move_type_id);

ALTER TABLE "GO_GAME".PATY_VERSION
ADD CONSTRAINT FK_PARTY
FOREIGN KEY (party_id)
REFERENCES "GO_GAME".PARTIES(party_id);

ALTER TABLE "GO_GAME".PARTY
ADD CONSTRAINT FK_PARTY_STATUS
FOREIGN KEY (party_status_id)
REFERENCES "GO_GAME".PARTY_STATUS(ps_id);

ALTER TABLE "GO_GAME".PARTY
ADD CONSTRAINT FK_PARTY_PREV_STATUS
FOREIGN KEY (party_prev_status_id)
REFERENCES "GO_GAME".PARTY_STATUS(ps_id);


ALTER TABLE "GO_GAME".PARTY_VERSION_MOVE_LINK
  ADD CONSTRAINT FK_MOVE
FOREIGN KEY (move_id)
    REFERENCES "GO_GAME".MOVES(move_id);

ALTER TABLE "GO_GAME".PARTY_VERSION_MOVE_LINK
ADD CONSTRAINT FK_PARTY_VERSION
FOREIGN KEY (party_id)
REFERENCES "GO_GAME".PARTIES(party_version_id);

ALTER TABLE "GO_GAME".MOVE_MOVE_COMMENT_LINK
ADD CONSTRAINT FK_MOVE
FOREIGN KEY (move_id)
REFERENCES "GO_GAME".MOVES(move_id);

ALTER TABLE "GO_GAME".MOVE_MOVE_COMMENT_LINK
ADD CONSTRAINT FK_COMMENT
FOREIGN KEY (comment_id)
REFERENCES "GO_GAME".COMMENT(comment_id);

CREATE TRIGGER party_version_trigger AFTER INSERT ON "GO_GAME".PARTIES
  REFERENCING NEW ROW AS newrow
INSERT INTO "GO_GAME".PARTY_VERSION(party_id,party_version,by_user_id,parent_party_version_id) VALUES (newrow.party_id, 1, 1,NULL);
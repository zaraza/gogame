import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.Move;
import game.go.domain.model.Party;
import game.go.domain.model.PartyVersion;
import game.go.domain.repository.MoveRepository;
import game.go.domain.repository.PartyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

import java.util.List;

/**
 * Created by Max on 25.10.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class MoveRepositoryTest {

    @Autowired
    MoveRepository moveRepository;

    @Autowired
    PartyRepository partyRepository;

    @Test
    @Transactional
    public void testCreateMove() {
        Party p = partyRepository.findOne(4L);
        List<PartyVersion> pv = p.getPartyVersions();
        assertEquals("PartyVersions",1,pv.size());
        PartyVersion mainVersion = pv.get(0);
        assertEquals("Moves for version",1,mainVersion.getPartyVersionMove().size());
    }

    @Test
    @Transactional
    public void testAloneMove() {
        Party p = partyRepository.findOne(5L);
        Move m = new Move(13,13,0);
        PartyVersion mainVersion = p.getPartyVersions().get(0);
        m.setParentPartyVersion(mainVersion);
        m  = moveRepository.saveAndFlush(m);

        m = moveRepository.findOne(m.getId());
        mainVersion = m.getParentPartyVersion();
        assertEquals("Chect liberty",4,m.getMoveLiberty());
        assertEquals("Check self root",m,m.getRootMove());
        assertEquals("Check increase moves in party version",2,mainVersion.getPartyVersionMove().size());
    }

    @Test
    @Transactional
    public void testSimpleMoveString() {
        Party p = partyRepository.findOne(6L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);
        assertEquals("Start size of moves",5,mainVersion.getPartyVersionMove().size());

    }
}

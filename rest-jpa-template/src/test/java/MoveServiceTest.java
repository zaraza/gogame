import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.Move;
import game.go.domain.model.Party;
import game.go.domain.model.PartyVersion;
import game.go.domain.repository.MoveRepository;
import game.go.domain.repository.PartyRepository;
import game.go.services.MoveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Max on 01.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class MoveServiceTest {

    @Autowired
    private MoveService moveService;

    @Autowired
    private MoveRepository moveRepository;

    @Autowired
    private PartyRepository partyRepository;

    @Test
    @Transactional
    public void testNeighbors(){
        Party p = partyRepository.findOne(6L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);
        List<Move> partyVersionMoves = mainVersion.getPartyVersionMove();
        Move m1 = partyVersionMoves.get(0);
        List<Move> neighbors = moveService.getNeighbors(m1);
       assertEquals(1, neighbors.size());

        Move m2 = partyVersionMoves.get(1);
        neighbors = moveService.getNeighbors(m2);
        assertEquals(2, neighbors.size());

        Move m5 = partyVersionMoves.get(4);
        neighbors = moveService.getNeighbors(m5);
        assertEquals(0, neighbors.size());
    }

    @Test
    @Transactional
    public void testAloneNeighbors() {
        Party p = partyRepository.findOne(5L);
        PartyVersion pv = p.getPartyVersions().get(0);
        assertEquals(1,pv.getPartyVersionMove().size());
        Move aloneMove = pv.getPartyVersionMove().get(0);

        assertEquals(0,moveService.getNeighbors(aloneMove).size());
    }

    @Test
    @Transactional
    public void testUnion() {
        Party p = partyRepository.findOne(7L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);
        List<Move> partyVersionMoves = mainVersion.getPartyVersionMove();
        assertEquals(4, partyVersionMoves.size());

        Move m1 = partyVersionMoves.get(0);
        Move m3 = partyVersionMoves.get(2);
        Move m3OldRoot = m3.getRootMove();

        int m1RootChildSize = m1.getRootMove().getChildMove().size();
        int m3RootChildSize = m3.getRootMove().getChildMove().size();

        moveService.union(m1,m3);
        //Look at one Root
        assertEquals(m1.getRootMove(),m3.getRootMove());
        //old root without child
        assertEquals(0,m3OldRoot.getChildMove().size());
        //all child move for m1 root without child - flat tree
        assertTrue(!m1.getRootMove().getChildMove().stream().anyMatch(m -> m.getChildMove().size() != 0));
        //total child: 1 from m1, 1 from m3 and m3 root as child = 3
        assertEquals(3,m1.getRootMove().getChildMove().size());
    }


    @Test
    @Transactional
    public void testMakeMoveLikeString(){
        Party p = partyRepository.findOne(8L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);

        assertEquals(0, mainVersion.getPartyVersionMove().size());

        Move m1 = new Move(5,5,0);
        m1.setParentPartyVersion(mainVersion);
        Move m2 = new Move(5,6,0);
        m2.setParentPartyVersion(mainVersion);
        Move m3 = new Move(5,7,0);
        m3.setParentPartyVersion(mainVersion);
        Move m4 = new Move(5,8,0);
        m4.setParentPartyVersion(mainVersion);

        moveService.makeMove(m1);
        assertEquals(1, mainVersion.getPartyVersionMove().size());

        moveService.makeMove(m2);
        //0.Total  size of moves - 2
        assertEquals(2,mainVersion.getPartyVersionMove().size());
        //1.Root must will be m1
        assertEquals(m1,m2.getRootMove());
        //2.Root must have 1 child
        assertEquals(1,m1.getChildMove().size());

        moveService.makeMove(m4);
        moveService.makeMove(m3);

        //Total
        assertArrayEquals(m1.getChildMove().toArray(), Arrays.asList(m2,m3,m4).toArray());

    }

    @Test
    @Transactional
    public void testLibertyCount() {
        Party p = partyRepository.findOne(10L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);

        assertEquals(0, mainVersion.getPartyVersionMove().size());

        //line

        Move m1 = new Move(5, 5, 0);
        m1.setParentPartyVersion(mainVersion);
        Move m2 = new Move(5, 6, 0);
        m2.setParentPartyVersion(mainVersion);
        Move m3 = new Move(5, 7, 0);
        m3.setParentPartyVersion(mainVersion);
        Move m4 = new Move(5, 8, 0);
        m4.setParentPartyVersion(mainVersion);

        moveService.makeMove(m1);
        assertEquals(4,m1.getRootMove().getMoveLiberty());
        moveService.makeMove(m2);
        assertEquals(6,m2.getRootMove().getMoveLiberty());

        //gap check
        moveService.makeMove(m4);
        assertEquals(4,m4.getRootMove().getMoveLiberty());
        assertEquals(6,m1.getRootMove().getMoveLiberty());

        moveService.makeMove(m3);
        assertEquals(10,m3.getRootMove().getMoveLiberty());
        assertEquals(m1.getRootMove(),m3.getRootMove());
        assertEquals(m1.getRootMove(),m4.getRootMove());

        //square
        Move m11 = new Move(10, 10, 0);
        m11.setParentPartyVersion(mainVersion);
        Move m12 = new Move(10, 11, 0);
        m12.setParentPartyVersion(mainVersion);
        Move m21 = new Move(11, 10, 0);
        m21.setParentPartyVersion(mainVersion);
        Move m22 = new Move(11, 11, 0);
        m22.setParentPartyVersion(mainVersion);

        moveService.makeMove(m11);
        assertEquals(4,m11.getRootMove().getMoveLiberty());
        moveService.makeMove(m12);
        assertEquals(6,m12.getRootMove().getMoveLiberty());
        moveService.makeMove(m21);
        //hmmmm, must by 7, but actual 8
        //this is empty triangle figure
        assertEquals(8,m21.getRootMove().getMoveLiberty());
        moveService.makeMove(m22);
        assertEquals(8,m22.getRootMove().getMoveLiberty());


        //square two black - two white
        Move m11B = new Move(13, 13, 0);
        m11B.setParentPartyVersion(mainVersion);
        Move m12B = new Move(13, 14, 0);
        m12B.setParentPartyVersion(mainVersion);
        Move m21W = new Move(14, 13, 1);
        m21W.setParentPartyVersion(mainVersion);
        Move m22W = new Move(14, 14, 1);
        m22W.setParentPartyVersion(mainVersion);

        moveService.makeMove(m11B);
        moveService.makeMove(m22W);
        assertEquals(4,m11B.getRootMove().getMoveLiberty());
        assertEquals(4,m22W.getRootMove().getMoveLiberty());

        moveService.makeMove(m12B);
        assertEquals(5,m12B.getRootMove().getMoveLiberty());
        assertEquals(3,m22W.getRootMove().getMoveLiberty());

        moveService.makeMove(m21W);
        assertEquals(4,m12B.getRootMove().getMoveLiberty());
        assertEquals(4,m21W.getRootMove().getMoveLiberty());
        assertNotEquals(m21W.getRootMove(), m12B.getRootMove());
    }

    @Test
    @Transactional
    public void testCaptureStone(){
        Party p = partyRepository.findOne(11L);
        PartyVersion mainVersion = p.getPartyVersions().get(0);

        assertEquals(0, mainVersion.getPartyVersionMove().size());

        //make
        Move mB = new Move(7,7,0);
        Move mWU = new Move(7,6,1);
        Move mWD = new Move(7,8,1);
        Move mWL = new Move(6,7,1);
        Move mWR = new Move(8,7,1);

        mB.setParentPartyVersion(mainVersion);
        mWU.setParentPartyVersion(mainVersion);
        mWD.setParentPartyVersion(mainVersion);
        mWR.setParentPartyVersion(mainVersion);
        mWL.setParentPartyVersion(mainVersion);

        moveService.makeMove(mB);
        moveService.makeMove(mWU);
        moveService.makeMove(mWD);
        moveService.makeMove(mWL);
        moveService.makeMove(mWR);

        assertEquals(0,mB.getRootMove().getMoveLiberty());
        assertTrue(mB.isDeleted());

        /* Create figure
         B B B
       B W W W B
       B W   W B
       B W W W B
         B B B
 */

        Move mBU1 = new Move(11,10,0,mainVersion);
        Move mBU2 = new Move(12,10,0,mainVersion);
        Move mBU3 = new Move(13,10,0,mainVersion);

        moveService.makeMove(mBU1);
        moveService.makeMove(mBU2);
        moveService.makeMove(mBU3);

        Move mBL1 = new Move(10,11,0,mainVersion);
        Move mBL2 = new Move(10,12,0,mainVersion);
        Move mBL3 = new Move(10,13,0,mainVersion);

        moveService.makeMove(mBL1);
        moveService.makeMove(mBL2);
        moveService.makeMove(mBL3);

        Move mBR1 = new Move(14,11,0,mainVersion);
        Move mBR2 = new Move(14,12,0,mainVersion);
        Move mBR3 = new Move(14,13,0,mainVersion);

        moveService.makeMove(mBR1);
        moveService.makeMove(mBR2);
        moveService.makeMove(mBR3);

        Move mBD1 = new Move(11,14,0,mainVersion);
        Move mBD2 = new Move(12,14,0,mainVersion);
        Move mBD3 = new Move(13,14,0,mainVersion);

        moveService.makeMove(mBD1);
        moveService.makeMove(mBD2);
        moveService.makeMove(mBD3);

        Move mWC11 = new Move(11,11,1,mainVersion);
        Move mWC13 = new Move(13,11,1,mainVersion);
        Move mWC31 = new Move(11,13,1, mainVersion);
        Move mWC33 = new Move(13,13,1,mainVersion);

        moveService.makeMove(mWC11);
        moveService.makeMove(mWC13);
        moveService.makeMove(mWC31);
        moveService.makeMove(mWC33);

        Move mWC12 = new Move(12,11,1,mainVersion);
        Move mWC21 = new Move(11,12,1,mainVersion);
        Move mwC32 = new Move(13,12,1,mainVersion);
        Move mwC21R = new Move(13,12,1,mainVersion);

        moveService.makeMove(mWC12);
        moveService.makeMove(mWC21);
        moveService.makeMove(mwC32);
        moveService.makeMove(mwC21R);

        //must 1, but 4 common liberties, need fix
        assertEquals(4,mwC21R.getRootMove().getMoveLiberty());

        Move mBZero =  new Move(12,12,0,mainVersion);
        moveService.makeMove(mBZero);

        assertEquals(4,mBZero.getRootMove().getMoveLiberty());
        assertEquals(0,mwC21R.getRootMove().getMoveLiberty());
        assertTrue(mwC21R.getRootMove().getChildMove().stream().anyMatch(m->!m.isDeleted()));






    }
}

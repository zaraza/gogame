import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.Party;
import game.go.domain.model.PartyState;
import game.go.domain.model.PartyStatePk;
import game.go.domain.model.PartyStatus;
import game.go.domain.repository.PartyRepository;
import org.junit.Test;
import org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Max on 21.10.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class PartyRepositoryTest {

    @Autowired
    PartyRepository partyRepository;

    @Test
    public void createPartyTest() {
        Party p = new Party();
        p.setPartyName("Test Created Party");
        p.setStartTime(new Date());
        p.setPartyStatus(new PartyStatus(PartyStatus.WAIT));
        p.setPartyPrevStatus(new PartyStatus(PartyStatus.CREATE));
        p.setSessionGuid("1111-1111-1111-9999");

        Party saved = partyRepository.save(p);
        assertNotNull(saved);
        assertEquals(saved.getPartyName(),p.getPartyName());
    }


    @Test
    public void changePartyStatus() {
        Party p = new Party();
        p.setPartyName("Party Change Status");
        p.setStartTime(new Date());
        p.setPartyStatus(new PartyStatus(PartyStatus.WAIT));
        p.setPartyPrevStatus(new PartyStatus(PartyStatus.CREATE));
        p.setSessionGuid("1111-1111-1111-9898");

        List<Party> waitParties = partyRepository.findByPartyStatus_IdIn(Arrays.asList(PartyStatus.WAIT));
        int startWaitSize = waitParties.size();

        assertEquals("Start wait party",3, waitParties.size());

        partyRepository.saveAndFlush(p);
        p = partyRepository.findOne(p.getId());
        PartyState ps = p.getPartyState();
        assertEquals("New party with create state",PartyStatePk.CREATE_PARTY,p.getPartyState().getPartyStateId());

        waitParties = partyRepository.findByPartyStatus_IdIn(Arrays.asList(PartyStatus.WAIT));

        assertEquals("Added new wait party",startWaitSize+1,waitParties.size());


        p.setPartyPrevStatus(p.getPartyStatus());
        p.setPartyStatus(new PartyStatus(PartyStatus.IN_PROGRESS));
        p = partyRepository.save(p);
        p = partyRepository.findOne(p.getId());

        assertEquals("new party with progress state", PartyStatePk.WAIT_AND_PROGRESS,p.getPartyState().getPartyStateId());


        waitParties = partyRepository.findByPartyStatus_IdIn(Arrays.asList(PartyStatus.WAIT));
        assertEquals("Start wait party size",startWaitSize,waitParties.size());
    }

    @Test
    public void waitList() {
        List<Party> waitParties = partyRepository.findByPartyStatus_IdIn(Arrays.asList(PartyStatus.WAIT));
        assertEquals(3,waitParties.size());
    }

    @Test
    public void userPartiesList() {

    }


    public void partyVersionTriggerTest() {
        Party p = new Party();
        p.setPartyName("Test For Trigger Party");
        p.setStartTime(new Date());
        p.setPartyStatus(new PartyStatus(PartyStatus.WAIT));
        p.setPartyPrevStatus(new PartyStatus(PartyStatus.CREATE));
        p.setSessionGuid("1111-1111-1111-9999");

        p = partyRepository.save(p);
        p = partyRepository.findOne(p.getId());
        assertEquals("Version of party",1,p.getPartyVersions().size());
    }
}

import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.Party;
import game.go.domain.repository.PartyRepository;
import game.go.dto.PartyDTO;
import game.go.services.PartyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Max on 18.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class PartyServiceTest {
    @Autowired
    private PartyService partyService;
    @Autowired
    private PartyRepository partyRepository;

    @Test
    @Transactional
    public void testCreatePartyFromDTO(){
        PartyDTO partyDTO = new PartyDTO("PARTY_TO_DTO", new Date(), 2L, 1L,"1000-000-1111");
        partyDTO = partyService.createParty(partyDTO);

        Party p = partyRepository.findOne(partyDTO.getId());
        assertEquals(partyDTO.getPartyName(), p.getPartyName());


    }
}

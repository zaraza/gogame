import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.Party;
import game.go.domain.model.PartyStatus;
import game.go.domain.repository.PartyRepository;
import game.go.dto.PartyDTO;
import game.go.template.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import static org.junit.Assert.*;
/**
 * Created by Max on 19.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class FacadeControllerTest {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {

        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        //this.mockMvc = MockMvcBuilders.standaloneSetup(facadeController).build();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }

    @Test
    public void simpleTest() throws Exception {
        this.mockMvc.perform(get("/greeting"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.content").value("Hello, World!"));
    }
    @Test
    public void createPartyTest() throws Exception{
        PartyDTO partyDTO = new PartyDTO("PartyDTO integration test",new Date(), PartyStatus.WAIT,PartyStatus.CREATE,"00000-000-000-00");

        int startSize = partyRepository.findAll().size();

        MvcResult mvc = this.mockMvc.perform(post("/process")
                            .contentType("application/json;charset=UTF-8")
                            .content(convertObjectToJsonBytes(partyDTO)))
                            .andExpect(status().isOk())
                            .andReturn();

        PartyDTO result = (PartyDTO)convertJsonBytesToObject(mvc,PartyDTO.class);
        List<Party> partyList = partyRepository.findAll();

        assertEquals(startSize + 1, partyList.size());

        Party savedPaty = partyRepository.findOne(result.getId());

        assertEquals(savedPaty.getStartTime(), result.getStartTime());
        assertEquals(savedPaty.getSessionGuid(),result.getSessionGuid());


    }


    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String s = mapper.writeValueAsString(object);
        return mapper.writeValueAsBytes(object);
    }

    public static Object convertJsonBytesToObject(byte[] array,Class type) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.readValue(array, type);
    }

    public static Object convertJsonBytesToObject(MvcResult result, Class type) throws IOException {
        return convertJsonBytesToObject(result.getResponse().getContentAsByteArray(), type);
    }
}

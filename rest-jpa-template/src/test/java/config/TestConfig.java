package config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by Max on 20.10.2015.
 */
@Configuration
@PropertySource("classpath:/application-test.properties")
public class TestConfig {
}

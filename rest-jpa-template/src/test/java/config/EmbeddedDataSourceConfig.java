package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Created by kostyuchenko on 19.10.2015.
 */
@Configuration
public class EmbeddedDataSourceConfig {
    @Bean(name = "dataSource")
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .addScript("classpath:sql/schema.sql")
                .addScript("classpath:sql/test-data.sql")
                .setType(EmbeddedDatabaseType.HSQL)
                .ignoreFailedDrops(true)
                .build();

    }
}
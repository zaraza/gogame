import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.dto.MoveDTO;
import game.go.logic.ClientMoveAnalyzer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Max on 27.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class ClientMoveAnalyzerTest {


    @Test
    public void testMakeMove(){
        ClientMoveAnalyzer ma = new ClientMoveAnalyzer();
        List<MoveDTO> moves = new ArrayList<>();
        MoveDTO w55 = new MoveDTO(5,5,0);
        ma.makeMove(w55,moves);
        MoveDTO b54 = new MoveDTO(5,4,1);
        ma.makeMove(b54,moves);

        assertEquals(3,b54.getMoveLiberty());
        assertEquals(3,w55.getMoveLiberty());

        MoveDTO b65 = new MoveDTO(6,5,1);
        ma.makeMove(b65,moves);

        MoveDTO b45 = new MoveDTO(4,5,1);
        ma.makeMove(b45,moves);

        MoveDTO b56 = new MoveDTO(5,6,1);
        ma.makeMove(b56,moves);

        assertEquals("Arounded checker must contain 0 liberty",0,w55.getMoveLiberty());

        MoveDTO w99=new MoveDTO(9,9,0);
        MoveDTO w910=new MoveDTO(9,10,0);

        ma.makeMove(w99,moves);
        ma.makeMove(w910,moves);


        assertEquals(3,w910.getMoveLiberty());
        assertEquals(6,w910.getRootMove().getMoveLiberty());

        MoveDTO b98=new MoveDTO(9,8,1);
        MoveDTO b89=new MoveDTO(8,9,1);
        MoveDTO b109 = new MoveDTO(10,9,1);
        MoveDTO b810 = new MoveDTO(8,10,1);
        MoveDTO b1010 = new MoveDTO(10,10,1);

        ma.makeMove(b98,moves);
        ma.makeMove(b89,moves);
        ma.makeMove(b109,moves);
        ma.makeMove(b810,moves);
        ma.makeMove(b1010,moves);

        assertEquals(1,w99.getRootMove().getMoveLiberty());


        moves.clear();

        MoveDTO b1313 = new MoveDTO(13,13,1);
        MoveDTO b1413 = new MoveDTO(14,13,1);
        MoveDTO b1314 = new MoveDTO(13,14,1);
        MoveDTO b1414 = new MoveDTO(14,14,1);

        ma.makeMove(b1313,moves);
        ma.makeMove(b1314,moves);
        ma.makeMove(b1413,moves);
        ma.makeMove(b1414,moves);

        assertEquals(8,b1313.getRootMove().getMoveLiberty());

        MoveDTO w1312 = new MoveDTO(13,12,0);
        MoveDTO w1412 = new MoveDTO(14,12,0);

        ma.makeMove(w1312,moves);
        assertEquals(7,b1313.getRootMove().getMoveLiberty());

        ma.makeMove(w1412,moves);
        assertEquals(6,b1313.getRootMove().getMoveLiberty());

    }
}

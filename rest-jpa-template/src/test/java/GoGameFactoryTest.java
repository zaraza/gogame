import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.PartyStatus;
import game.go.domain.states.party.PartyCreateState;
import game.go.dto.PartyDTO;
import game.go.services.GoGameFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.Date;

/**
 * Created by Max on 21.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
public class GoGameFactoryTest {
    @Autowired
    GoGameFactory goGameFactory;

    @Test
    public void partyCreateStateFromPartyDTOTest(){
        PartyDTO partyDTO = new PartyDTO("PartyDTO integration test",new Date(), PartyStatus.WAIT,PartyStatus.CREATE,"00000-000-000-00");
        PartyCreateState  pcs = (PartyCreateState)goGameFactory.getDecoratedItem(partyDTO);
        pcs.execute();
        pcs.getResult();
        assertNotEquals(0L,((PartyDTO)pcs.getResult()).getId());

    }
}

import config.EmbeddedDataSourceConfig;
import config.TestConfig;
import game.go.config.RepositoryConfiguration;
import game.go.domain.model.PartyState;
import game.go.domain.repository.PartyStateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Max on 02.10.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, RepositoryConfiguration.class, EmbeddedDataSourceConfig.class})
@ActiveProfiles("test")
public class PartyStateRepositoryTest {

    @Autowired
    PartyStateRepository partyStateRepository;


    @Test
    public void testDataLoad() {
        List<PartyState> partyStateList = partyStateRepository.findAll();
        org.junit.Assert.assertEquals(7, partyStateList.size());
//        Assert.assertNotEquals(1L,2L);
    }


}

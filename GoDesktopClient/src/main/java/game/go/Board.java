package game.go;

import game.go.dto.MoveDTO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by Max on 13.12.2015.
 */
public class Board extends JPanel{
    ArrayList<MoveDTO> checkers = new ArrayList<>();

    private void drawBords(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g.drawRect(25, 25, 450, 450);
        for (int i = 1; i <= 17; i++) {
            g2d.drawLine(25, (25 + i * 25), 475, (25 + i * 25));
            g2d.drawLine((25 + i * 25), 25, (25 + i * 25), 475);
        }
        int r = 12;
    }

    private void drawCheckers(Graphics g){
        if(checkers!=null && checkers.size()>0){
            for(MoveDTO checker: checkers){
                Graphics2D g2d = (Graphics2D)g;
                if (checker.getColor() == 1) {
                    g2d.setPaint(Color.WHITE);
                } else {
                    g2d.setPaint(Color.BLACK);
                }

                Stroke stroke = new BasicStroke(10.0f);
                g2d.setStroke(stroke);
                g2d.fill(Utils.createVisualPoint(checker, 12));
            }
            System.out.println("Draw checkers. Size:"+checkers.size());
        }

    }

    public void paint(Graphics g) {
        super.paintComponent(g);
        drawBords(g);
        drawCheckers(g);
    }

    public ArrayList<MoveDTO> getCheckers() {
        return checkers;
    }

    public void setCheckers(ArrayList<MoveDTO> checkers) {
        this.checkers = checkers;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(280,280);
    }
}

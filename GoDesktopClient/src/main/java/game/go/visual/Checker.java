package game.go.visual;

import game.go.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * Created by Max on 12.01.2016.
 */
public class Checker extends JComponent {
    private int diameter = 50;
    private Color color = Color.BLACK;

    public Checker(){}
    public Checker(int x, int y, int diameter,int color){
        super();
        this.setLocation(x, y);
        this.diameter=diameter;
        this.setSize(diameter, diameter);
    }

    public Checker(int x, int y, int color){
        super();
        this.setLocation(x, y);
        if (color == 1) {
            this.color = Color.WHITE;
        } else {
            this.color = Color.BLACK;
        }
        this.setSize(this.diameter, this.diameter);
    }

    public Checker(int x, int y, Color color){
        super();
        this.setLocation(x, y);
        this.color = color;
        this.setSize(this.diameter, this.diameter);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Ellipse2D.Double circle =
                new Ellipse2D.Double(this.getX() * diameter/2 - diameter/2,
                        this.getY() * diameter/2 - diameter/2, diameter, diameter);

        Graphics2D g2d = (Graphics2D)g;
        g2d.setColor(color);
        Stroke stroke = new BasicStroke(10.0f);
        g2d.setStroke(stroke);
        g2d.fill(circle);
    }
}

package game.go;

/**
 * Created by Max on 13.12.2015.
 */
import game.go.dto.MoveDTO;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;


public class Utils {
    int[] x;
    int[] y;
    public Utils() {
        super();
    }

    public void fillArrays(int[] dataX, int[] dataY){
        this.x = dataX;
        this.y = dataY;
    }

    public static int calculateNearest(int start, int end, int step, int randomX){
        int halfStep = step/2;
        int del = randomX/step;
        int mod = randomX%step;
        if(mod>=halfStep){ del++; }
        //System.out.println("Coord:"+randomX+", nearest:"+del*step);
        return del*step;

    }



        public static Ellipse2D.Double createVisualPoint(MoveDTO move, int radius){
        Ellipse2D.Double circle =
                new Ellipse2D.Double(move.getX() * 25 - radius,
                        move.getY() * 25 - radius, radius * 2, radius * 2);
        return circle;
    }
}


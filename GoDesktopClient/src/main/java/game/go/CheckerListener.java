package game.go;

import game.go.dto.MoveDTO;
import game.go.logic.ClientMoveAnalyzer;
import game.go.logic.MoveAnalyzer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

/**
 * Created by Max on 13.12.2015.
 */
public class CheckerListener extends MouseAdapter {
    @Override
    public void mouseClicked(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)) {
            //calculate nearest chords
            int x = Utils.calculateNearest(25, 475, 25, e.getX());
            int y = Utils.calculateNearest(25, 475, 25, e.getY());

            Board board = (Board) e.getSource();
            int currentSize = board.getCheckers().size();

            MoveDTO checker = new MoveDTO(x / 25, y / 25, currentSize % 2);
            ClientMoveAnalyzer ma = new ClientMoveAnalyzer();

            ma.makeMove(checker, board.getCheckers());

            board.repaint();
        }

        if(SwingUtilities.isRightMouseButton(e)){
            System.out.println("Right click!");

            Board board = (Board)e.getSource();
            JPanel mainPanel = (JPanel)board.getParent();
            mainPanel.getComponentCount();
            JLabel infoLabel = (JLabel)mainPanel.getComponent(1);
            infoLabel.setText("Changed");
            infoLabel.setForeground(Color.BLUE);

            int x = Utils.calculateNearest(25, 475, 25, e.getX());
            int y = Utils.calculateNearest(25, 475, 25, e.getY());
            for(MoveDTO move:board.getCheckers()){
                if(move.getX()*25==x){
                    if(move.getY()*25==y){
                        infoLabel.setText("You click on "+x+","+y+", root moveLiberty="+move.getRootMove().getMoveLiberty());
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}

package game.go;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by Max on 13.12.2015.
 */
public class GoClient {
    private JPanel rootPanel;
    private JPanel gamePanel;
    private JPanel infoPanel;
    private JPanel moveScroll;
    private JScrollBar scrollBar1;
    private JLabel moveCount;
    private JTextField currentMoveNumber;
    private JButton button1;

    private void createComponentUI(){
        JFrame frame = new JFrame("Go Board");
        frame.setSize(800,600);
//        frame.setLocationRelativeTo(null);
        // frame.getContentPane().setLayout(new GridBagLayout());

        JPanel mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setName("MainPanel");
        mainPanel.setPreferredSize(new Dimension(800,800));
        frame.getContentPane().add(mainPanel);
        GridBagConstraints c = new GridBagConstraints();


        Board board = new Board();
        board.addMouseListener(new CheckerListener());
        c.weightx=1.0;
        c.weighty=1.0;
        c.fill=GridBagConstraints.BOTH;
        c.gridx=0;
        c.gridy=0;
        c.gridwidth=4;
        c.gridheight=4;


        mainPanel.add(board,c);

        Font font = new Font("Verdana", Font.PLAIN, 12);
        JLabel topLabel = new JLabel("Top");
        topLabel.setVerticalAlignment(JLabel.TOP);
        topLabel.setHorizontalAlignment(JLabel.CENTER);
        topLabel.setFont(font);
        topLabel.setForeground(Color.GREEN);
        c.weightx=1.0;
        c.weighty=1.0;
        c.fill=GridBagConstraints.BOTH;
        c.gridx=5;
        c.gridy=3;
        c.gridwidth=2;
        c.gridheight=4;

        mainPanel.add(topLabel,c);
        c.fill=GridBagConstraints.HORIZONTAL;
        c.gridx=3;
        c.gridy=0;
        c.weightx=0.5;
        c.gridwidth=2;
        c.gridheight=2;

        JPanel checkerPanel = new JPanel(new BorderLayout());
        checkerPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        c.fill=GridBagConstraints.HORIZONTAL;
        c.gridx=5;
        c.gridy=6;
        c.weightx=0.25;
        c.weighty=0.25;
        c.gridwidth=4;
        c.gridheight=4;
        JLabel checkerX = new JLabel("X of Move");
        JButton submit = new JButton("Submit Form");

        checkerPanel.add(checkerX);
        checkerPanel.add(submit);
        //mainPanel.add(checkerPanel,c);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);
    }

    private void checkIDEACodeUI(){
        rootPanel = new JPanel();
        rootPanel.setLayout(new GridBagLayout());
        gamePanel = new JPanel();
        gamePanel.setLayout(new GridBagLayout(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        gbc.weightx = 0.8;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        rootPanel.add(gamePanel, gbc);
        gamePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        infoPanel = new JPanel();
        infoPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        gbc.weightx = 0.3;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        rootPanel.add(infoPanel, gbc);
        infoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        button1 = new JButton();
        button1.setText("Button");
        infoPanel.add(button1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        infoPanel.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        infoPanel.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        moveScroll = new JPanel();
        moveScroll.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.BOTH;
        rootPanel.add(moveScroll, gbc);
        scrollBar1 = new JScrollBar();
        scrollBar1.setOrientation(0);
        moveScroll.add(scrollBar1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER, -1, -1));
        moveScroll.add(panel1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        currentMoveNumber = new JTextField();
        currentMoveNumber.setColumns(4);
        panel1.add(currentMoveNumber);
        moveCount = new JLabel();
        moveCount.setHorizontalAlignment(0);
        moveCount.setText("Label");
        panel1.add(moveCount);
    }
    public static void main(String[] args) {

    }
}

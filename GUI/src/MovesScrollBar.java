import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Max on 19.01.2016.
 */
public class MovesScrollBar extends JScrollBar implements Observer{


    @Override
    public void update(Observable o, Object arg) {
        int value = Integer.parseInt((String) arg);
        if(value>this.getMaximum()){
            this.setMaximum(value);
            this.setValue(value);
        } else if(value<=this.getMaximum()){
            this.setValue(value);
        }
    }
}

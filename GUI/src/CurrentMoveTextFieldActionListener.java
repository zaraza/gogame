import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by maxim.kostyuchenko on 19.01.2016.
 */
public class CurrentMoveTextFieldActionListener extends Observable implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {

        JTextField currentMove = (JTextField) e.getSource();
        setChanged();
        notifyObservers(currentMove.getText());

    }

}

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by maxim.kostyuchenko on 19.01.2016.
 */
public class TotalMoveCountLabel extends JLabel implements Observer{


    @Override
    public void update(Observable o, Object arg) {

        if(arg instanceof String){
            this.setText("/"+(String) arg);
            this.repaint();
        }
    }
}

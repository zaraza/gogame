import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Max on 19.01.2016.
 */
public class CurrentMoveTextField extends JTextField implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        this.setText((String) arg);
    }

//    private Observable currentMoveObserver = new Observable(){
//        public void notifyObservers(Object arg) {
//            setChanged();
//            super.notifyObservers(arg);
//        }
//    };
//
//    public void addCurrentMoveObserver(Observer o)
//    {
//        currentMoveObserver.addObserver(o);
//    }
}

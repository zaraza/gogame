/**
 * Created by maxim.kostyuchenko on 15.01.2016.
 */
public class Utils {

    public static int calculateNearest(int start, int end, int step, int randomX){
        int halfStep = step/2;
        int del = randomX/step;
        int mod = randomX%step;
        if(mod>=halfStep){ del++; }
        //System.out.println("Coord:"+randomX+", nearest:"+del*step);
        return del*step;

    }
}

import game.go.dto.MoveDTO;

import javax.swing.*;
import java.awt.*;

/**
 * Created by maxim.kostyuchenko on 13.01.2016.
 */
public class JChecker extends JComponent {

    private static final long serialVersionUID = 1L;
    private int diameter;
    private Color color = Color.BLUE;
    private MoveDTO moveDTO;

    public JChecker() { }
    public JChecker(int x, int y, int diameter)
    {
        super();
        this.diameter = diameter;
        this.setLocation(x, y);
        this.setSize(diameter, diameter);
    }

    public JChecker(MoveDTO moveDTO, int diameter){
        super();
        this.moveDTO = moveDTO;
        this.diameter = diameter;
        this.color = moveDTO.getColor()==1?Color.BLACK:Color.WHITE;
        this.setLocation(moveDTO.getX()*diameter-diameter/2,moveDTO.getY()*diameter-diameter/2);
        this.setSize(diameter,diameter);

    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g.create();
        int x = getInsets().left;
        int y = getInsets().top;
        int w = getWidth() - getInsets().left - getInsets().right;
        int h = getHeight() - getInsets().top - getInsets().bottom;

        g2.setPaint(color);
        g2.fillOval(x, y, w, h);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(diameter,diameter);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

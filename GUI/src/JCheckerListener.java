import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by maxim.kostyuchenko on 13.01.2016.
 */
public class JCheckerListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if(SwingUtilities.isRightMouseButton(e)){
            JChecker checker = (JChecker)  e.getSource();
            System.out.print("coord:"+checker.getX()+","+checker.getY());
            checker.setColor(Color.GREEN);
            checker.repaint();
        }
    }
}

import game.go.dto.MoveDTO;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observer;


/**
 * Created by maxim.kostyuchenko on 13.01.2016.
 */
public class GameBoard extends JPanel implements Observer{
    public ArrayList<JChecker> checkers = new ArrayList<>();
    private ArrayList<MoveDTO> moveDTOs = new ArrayList<>();

    private int currentMove = 0;

    public int getCurrentMove() {
        return currentMove;
    }

    public void setCurrentMove(int currentMove) {
        this.currentMove = currentMove;
    }

    public GameBoard(){super();}

    public GameBoard(LayoutManager layout){
        super(layout);
    }

    private void drawBoards(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g.drawRect(25, 25, 450, 450);
        for (int i = 1; i <= 17; i++) {
            g2d.drawLine(25, (25 + i * 25), 475, (25 + i * 25));
            g2d.drawLine((25 + i * 25), 25, (25 + i * 25), 475);
        }
        int r = 12;
    }

    private void drawCheckers(Graphics g) {
        if(moveDTOs.size()>0){
            System.out.println("Total count:"+moveDTOs.size()+", current move"+currentMove);
            removeAll();
            for(int i=0; i<currentMove;i++) {
                MoveDTO moveDTO = moveDTOs.get(i);

                JChecker checker=new JChecker(moveDTO,25);
                checker.addMouseListener(new JCheckerListener());

                add(checker);
            }
        }
    }

    //Ivan's useful rework. If overrrided paint(Graphics g), checkers doesn't display on grid
    @Override protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawBoards(g);
        drawCheckers(g);

    }

    public ArrayList<JChecker> getCheckers() {
        return checkers;
    }

    public void setCheckers(ArrayList<JChecker> checkers) {
        this.checkers = checkers;
    }

    public ArrayList<MoveDTO> getMoveDTOs() {
        return moveDTOs;
    }

    public void setMoveDTOs(ArrayList<MoveDTO> moveDTOs) {
        this.moveDTOs = moveDTOs;
    }

    @Override
    public void update(java.util.Observable o, Object arg) {
        this.currentMove = Integer.parseInt((String) arg);
        System.out.println("Receive notify with arg:"+arg);

        repaint();

    }
}

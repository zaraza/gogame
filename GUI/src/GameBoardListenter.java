import game.go.dto.MoveDTO;
import game.go.logic.ClientMoveAnalyzer;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by maxim.kostyuchenko on 13.01.2016.
 */
public class GameBoardListenter extends MouseAdapter {
    JLabel totalMoveCountLabel;

    public GameBoardListenter(){super();}

    public GameBoardListenter(JLabel totalMoveCountLabel){
        this.totalMoveCountLabel = totalMoveCountLabel;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);

        if(SwingUtilities.isLeftMouseButton(e)) {
            //calculate nearest node of grid
            int x = Utils.calculateNearest(25, 475, 25, e.getX());
            int y = Utils.calculateNearest(25, 475, 25, e.getY());
            GameBoard gameBoard = (GameBoard) e.getSource();

            MoveDTO moveDTO = new MoveDTO(x/25,y/25,gameBoard.getMoveDTOs().size()%2);
            ClientMoveAnalyzer cma = new ClientMoveAnalyzer();

            cma.makeMove(moveDTO,gameBoard.getMoveDTOs());

            gameBoard.setCurrentMove(gameBoard.getMoveDTOs().size());

            //Update total count label
            //Used Observer Pattern, update TotalMoveCountLabel
            totalCheckersCountObservable.notifyObservers(Integer.toString(gameBoard.getMoveDTOs().size()));

            gameBoard.revalidate();
            gameBoard.repaint();


        }

    }

    private Observable totalCheckersCountObservable = new Observable() {
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    };

    public void addTotalCheckersCountObserver(Observer o)
    {
        totalCheckersCountObservable.addObserver(o);
    }

}

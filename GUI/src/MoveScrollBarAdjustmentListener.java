import javax.swing.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Observable;

/**
 * Created by Max on 19.01.2016.
 */
public class MoveScrollBarAdjustmentListener extends Observable implements AdjustmentListener {
    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        int value = e.getValue();
        this.setChanged();
        this.notifyObservers(String.valueOf(value));

    }
}
